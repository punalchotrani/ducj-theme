<?php



 get_header(); ?>
 <?php if ( is_home() ) { ?>


<div class="row3"> 


<div id="slider">

<?php get_template_part(''.$slides = of_get_option('slider_select', 'nivo').''); ?>
 <?php }?>  		
            
</div>
<div class="slide_shadow"></div>


</div>
</div>



<!--MIDROW STARTS-->


<?php if ( is_home() ) { ?>

<div class="warp row "> 

<div class="services-wrap large-12 columns">



    
<?php if(of_get_option('blocks_checkbox','ducj') == "1"){ ?>
<div class="midrow">

 <div class="midrow_wrap">       
        <div class="midrow_blocks">  
       
        <div class="midrow_blocks_wrap">
         <i class="fa <?php echo of_get_option('block1_logo'); ?> fa-3x icon"></i> 
        <?php if ( of_get_option('block1_textarea') ) { ?>
        <div class="midrow_block">
        <div class="mid_block_content">
        <h3><?php echo of_get_option('block1_text'); ?></h3>
        <p><?php echo of_get_option('block1_textarea'); ?></p>
    
        </div>
         <?php if ( of_get_option('block1_link') ) { ?><a href="<?php echo of_get_option('block1_link'); ?>" class="blocklink"><?php _e('More', 'ducj'); ?></a><?php } ?>
        </div></div>
         <div class="shadow"><img  src="<?php echo get_template_directory_uri(); ?>/images/service_shadow.png" alt="<?php the_title_attribute(); ?>" /></div>
       
        </div>
        
         
        <?php } ?>
            
        <div class="midrow_blocks">  
        
       <div class="midrow_blocks_wrap">
        <i class="fa <?php echo of_get_option('block2_logo'); ?> fa-3x icon"></i>
        <?php if ( of_get_option('block2_textarea') ) { ?>
        <div class="midrow_block">
        
        <div class="mid_block_content">
        <h3><?php echo of_get_option('block2_text'); ?></h3>
        <p><?php echo of_get_option('block2_textarea'); ?></p>
     
        </div>
        <?php if ( of_get_option('block2_link') ) { ?><a href="<?php echo of_get_option('block2_link'); ?>" class="blocklink"><?php _e('More', 'ducj'); ?></a><?php } ?>
        </div>
      
        
        </div>
           <div class="shadow"><img  src="<?php echo get_template_directory_uri(); ?>/images/service_shadow.png" alt="<?php the_title_attribute(); ?>" /></div>
       
        
        </div>
        
        <?php } ?>
        
        
         <div class="midrow_blocks">  
        
       <div class="midrow_blocks_wrap">
        <i class="fa <?php echo of_get_option('block3_logo'); ?> fa-3x icon"></i>
        
        <?php if ( of_get_option('block3_textarea') ) { ?>
        <div class="midrow_block">
        
        <div class="mid_block_content">
        <h3><?php echo of_get_option('block3_text'); ?></h3>
        <p><?php echo of_get_option('block3_textarea'); ?></p>
    
        </div>
         <?php if ( of_get_option('block3_link') ) { ?><a href="<?php echo of_get_option('block3_link'); ?>" class="blocklink"><?php _e('More', 'ducj'); ?></a><?php } ?>
        </div></div>
        <div class="shadow"><img  src="<?php echo get_template_directory_uri(); ?>/images/service_shadow.png" alt="<?php the_title_attribute(); ?>" /></div>
        
        </div>
         <?php } ?>
         
         
         <div class="midrow_blocks">  
        
       <div class="midrow_blocks_wrap">
        <i class="fa <?php echo of_get_option('block4_logo'); ?> fa-3x icon"></i>
         
        <?php if ( of_get_option('block4_textarea') ) { ?>
        
        
         
        <div class="mid_block_content">
        
        <h3><?php echo of_get_option('block4_text'); ?></h3>
        <p><?php echo of_get_option('block4_textarea'); ?></p>
     
        </div>
        <?php if ( of_get_option('block4_link') ) { ?><a href="<?php echo of_get_option('block4_link'); ?>" class="blocklink"><?php _e('More', 'ducj'); ?></a><?php } ?>
        </div>
        <div class="shadow"><img  src="<?php echo get_template_directory_uri(); ?>/images/service_shadow.png" alt="<?php the_title_attribute(); ?>" /></div>
        <?php } ?>
        
        
<?php } ?>


  
         
        </div>
        
        
        </div>
        
</div>
        </div>
        
   
<?php }?>


</div> 

<!--MIDROW END-->


<!-- place welcome banner here-->



<?php if ( is_home() ) { ?>
<?php if(of_get_option('welcome_enable','ducj') == "1"){ ?>
<div class=" warp row">
<div class="large-12">
<section id="callout">
 
<?php if ( of_get_option('ducj_welcome') ) : ?>
            
            <!-- Start Callout section -->
            
            <?php echo apply_filters('the_content', of_get_option('ducj_welcome')); ?>
              <?php if ( of_get_option('call_link') ) { ?>
            <a href="<?php echo of_get_option('call_link'); ?>"><?php _e('Get it Now', 'ducj'); ?></a>
          <?php }?>
 </section>
  
  </div></div>        

            <?php endif; ?>
            <?php }?>
            <?php } ?> 
</div></div>




<!-- place welcome banner here--><!-- END Call to action  -->  


 <!-- Start Call to action -->
 
        
              <!-- END Call to action  -->  
          

<!--recent work-->
<?php if ( is_home() ) { ?>
<div class="warp row "> 

<div class="large-12 columns">


<?php if(of_get_option('recentwork_checkbox','ducj') == "1"){ ?>


<?php get_template_part('parts/mid','contant'); ?>



<?php }?>

</div></div>		
<?php }?></div>


<!--recent work end-->




<!--LATEST POSTS-->

<?php if ( is_home() ) { ?>
 <div class=" warp row "> 
<div class=" large-12 columns ">
<div class="title">
<h2 class="blue"><?php echo of_get_option('latest_blog'); ?></h2></div>	
	<?php if(of_get_option('latstpst_checkbox') == "1")
	
	{ ?><?php get_template_part(''.$os_lays = of_get_option('layout1_images','layout1').''); ?><?php } else { ?><?php } ?>
<?php } else { ?>
	<?php get_template_part(''.$os_lays = of_get_option('layout1_images', 'layout1').''); ?>
    
<?php } ?> 
 <?php get_template_part('pagination'); ?> 
</div>

</div></div></div></div>

 <div class="warp row "> 

<div class="large-12 columns">

<?php get_template_part('parts/our','client'); ?>

</div></div>
      
<!--LATEST POSTS END-->

<!--Layout END-->

 <!-- Start Call to action -->


<?php if ( is_home() ) { ?>
<?php if(of_get_option('callout_enable','ducj') == "1"){ ?>
<div class="warp row">
<div class="large-12 columns">
<section id="callout2">
<a class="call" href="<?php echo of_get_option('call1_link'); ?>"><?php echo of_get_option('call1_linkname'); ?></a>
           
<div class="contents">
<?php if ( of_get_option('ducj_call1') ) : ?>
            
           
            <?php echo apply_filters('the_content', of_get_option('ducj_call1')); ?>
             
            </div>
            
            
         
 </section>
  </div></div>       

            <?php endif; ?>
            
            <?php } ?> 
            <?php } ?> 
</div></div>  

          <!-- END Call to action  -->  


<?php get_footer(); ?>