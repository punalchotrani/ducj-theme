<?php	

/**
 * ducj functions and definitions
 *
 * For more information on hooks, actions, and filters, @link http://codex.wordpress.org/Plugin_API
 *
* @package PunalThemes
 * @subpackage ducj
 * @since ducj 1.5
 */

/*
 * Set up the content width value based on the theme's design.
 *
 */

if ( ! isset( $content_width ) )
	$content_width = 630;

/**
 * Adds support for a custom header image.
 */
require get_template_directory() . '/admin/custom-header.php';

//Load Other CSS files

function ducj_other_css() { 
if ( !is_admin() ) {	
wp_enqueue_style( 'ducj_other', get_template_directory_uri() . '/css/foundation.css' );
//wp_enqueue_style( 'ducj_other', get_template_directory_uri() . '/css/gallery.css' );




}  }
add_action('wp_enqueue_scripts', 'ducj_other_css');	


 
 



function ducj_fonts_css() { 
if ( !is_admin(
) ) {
{ ?>
<?php wp_enqueue_style('customfont',get_template_directory_uri().'/fonts/'.$os_fonts = of_get_option('font_select', 'lora','raleway' ).'.css'); }
include(get_template_directory() . '/fonts/font.php');
	}
	
}
add_action('wp_enqueue_scripts', 'ducj_fonts_css');	

//Load Custom CSS
function ducj_customstyle() { ?>
<?php if(of_get_option('sldrtxt_checkbox') == "0"){ ?>
<style type="text/css">
body .nivo-caption {
	display: none!important;
}
</style>
<?php } ?>


<style type="text/css">
/*Secondary Elements Color*/

#site-title a,#site-title2 a,#site-title3 a{
font-size:<?php echo of_get_option('fontsize_select');
?>!important;
}

#sub_banner{
background-color:<?php echo of_get_option('pagehead_colorpicker');
?>!important;}


#sub_banner h1{
color:<?php echo of_get_option('pageheadtext_colorpicker');
?>!important;}

.postitle, .postitle a,.postitle2 a, .widgettitle,.widget-title, .entry-title a, .widgettitle2, #reply-title, #comments span, .catag_list a, .nivo-caption a, .nivo-caption,.entry-title,.title h2.blue,.title h2.green,#sub_banner h1 ,.content_blog .post_title a{
color:<?php echo of_get_option('title_colorpicker');
?>!important;
border-color:<?php echo of_get_option('title_colorpicker');
?>!important;
}
 #copyright, #navmenu ul li ul li,  #today,#menu_wrap2,menu_wrap3{
background-color:<?php echo of_get_option('menu_colorpicker');
?>!important;
}

#footer{
background-color:<?php echo of_get_option('wedgets_colorpicker');
?>!important;
}
#footer .widgets .widget ul li a{
color:<?php echo of_get_option('wedtext_colorpicker');
?>!important;
}

.midrow_blocks_wrap{
background-color:<?php echo of_get_option('serv_colorpicker');
?>!important;
}
.midrow_block p,.services-wrap p,#callout h1,#callout h2,#callout h3,#callout h4,#callout h5,#callout h6,#callout p,#callout a,.mid_block_content h3 {
color:<?php echo of_get_option('welltext_colorpicker');
?>!important;
}
.midrow_blocks_wrap{
color:<?php echo of_get_option('serlogo_colorpicker');
?>!important;
}
#copyright,#copyright a, #today,#menu_wrap2,menu_wrap3, #navmenu li a {
color:<?php echo of_get_option('menutext_colorpicker');
?>!important;

}



#branding,#branding2,#branding3,#branding4 {
background-color:<?php echo of_get_option('header_colorpicker');
?>!important;
}

#site-title a,.desc,#site-title2 a,#site-title3 a,.desc2 {
color:<?php echo of_get_option('headertext_colorpicker');
?>!important;
}

.post_info,.post_info a,post_author,.blog_style_b1 .post_content p,#sidebar .widgets .widget li,#sidebar .widgets .widget li a,.wp-pagenavi a,.share,h4.heading ,.logged-in-as,.form-allowed-tags,#comments,a.comments_template,.related-posts .related-inner a,#respond label,.postcontent p ,.lay2 .hentry p,.lay2 .hentry h3{
color:<?php echo of_get_option('text_colorpicker');
?>!important;
}

.warp,#slider,#content,.top-content,.post_content,.related,#respond,#sidebar .widgets .widget,.lay1 .hentry ,.caroufredsel_wrapper,.work-carousel li,.lay2,.lay2 .hentry ,.post_info_wrap   {
background-color:<?php echo of_get_option('bg_colorpicker');
?>!important;
}

.midrow_blocks_wrap:hover,.view a.info:hover,#navmenu ul > li ul li:hover,#submit:hover,.link:hover,#searchsubmit {
background-color:<?php echo of_get_option('hover_colorpicker');
?>!important; background:<?php echo of_get_option('hover_colorpicker');?>!important;

}
.ch-info a:hover,.widget_tag_cloud a:hover,.post_info a:hover,.post_views a:hover,
.post_comments a:hover,.wp-pagenavi:hover, .alignleft a:hover, .wp-pagenavi:hover ,.alignright a:hover,.comment-form a:hover,.proj-description a:hover,#comments:hover,.buttonPro:hover{
color:<?php echo of_get_option('hover_colorpicker');
?>!important;}

.nivo-caption h3 ,.nivo-caption p{opacity:<?php echo of_get_option('opacity_colorpicker');
?>!important;}

.nivo-caption h3 {font-size:<?php echo of_get_option('titels_colorpicker');
?>!important;}
.nivo-caption p {font-size:font-size:<?php echo of_get_option('caption_colorpicker');
?>!important;}
#callout2{background-color:<?php echo of_get_option('call1_colorpicker');
?>!important;}
#callout {background-color:<?php echo of_get_option('callcolor_colorpicker');
?>!important;}

#sub_banner h1 {font-size:<?php echo of_get_option('headerfs_select');
?>!important;}

</style>
<?php }

add_action( 'wp_head', 'ducj_customstyle' );



//Load Java Scripts to header
function ducj_head_js() { 
if ( !is_admin() ) {
wp_enqueue_script('jquery');
wp_enqueue_script('ducj_js',get_template_directory_uri().'/other2.js');
wp_enqueue_script('ducj_other',get_template_directory_uri().'/js/other.js');





if(of_get_option('slider_select') == "nivo"){ wp_enqueue_script('ducj_nivo',get_template_directory_uri().'/js/jquery.nivo.js');}
if(of_get_option('disslight_checkbox') == "0")
if ( is_singular() ) wp_enqueue_script( 'comment-reply' );
if(of_get_option('slider_select') == "slidorion"){ wp_enqueue_script('osiris_slidorion',get_template_directory_uri().'/js/jquery.slidorion.js');}

}
}
add_action('wp_enqueue_scripts', 'ducj_head_js');

//Load Java Scripts to Footer
add_action('wp_footer', 'ducj_load_js');

function ducj_load_js() { ?>
<?php if(of_get_option('slider_select') == "nivo"){ ?>

<script type="text/javascript">
    jQuery(window).load(function() {
		// nivoslider init
		jQuery('#nivo').nivoSlider({
				effect: '<?php echo of_get_option('effcet_select'); ?>',
				animSpeed:700,
				pauseTime:<?php echo of_get_option('sliderspeed_text'); ?>,
				startSlide:0,
				slices:10,
				directionNav:true,
				directionNavHide:true,
				controlNav:true,
				controlNavThumbs:false,
				keyboardNav:true,
				pauseOnHover:true,
				captionOpacity:0.8,
				afterLoad: function(){
						if (jQuery(window).width() < 480) {
					jQuery(".nivo-caption").animate({"opacity": "1", "right":"0"}, {easing:"easeOutBack", duration: 500});
						}else{
					jQuery(".nivo-caption").animate({"opacity": "1", "right":"11%"}, {easing:"easeOutBack", duration: 500});	
					jQuery(".nivo-caption").has('.sld_layout3').addClass('sld3wrap');
							}
				},
				beforeChange: function(){
					jQuery(".nivo-caption").animate({right:"-500px"}, {easing:"easeInBack", duration: 500});
					//jQuery(".nivo-caption").delay(400).removeClass('sld3wrap');
					jQuery('.nivo-caption').animate({"opacity": "0"}, 100);
					jQuery('.nivo-caption').delay(500).queue(function(next){
						jQuery(this).removeClass("sld3wrap");next();});

				},
				afterChange: function(){
						if (jQuery(window).width() < 480) {
					jQuery(".nivo-caption").animate({"opacity": "1", "right":"0"}, {easing:"easeOutBack", duration: 500});
						}else{
					jQuery(".nivo-caption").animate({"opacity": "1", "right":"11%"}, {easing:"easeOutBack", duration: 500});	
					jQuery(".nivo-caption").has('.sld_layout3').addClass('sld3wrap');	
							}
				}
			});
	});
</script>

<?php } ?>

<?php if(of_get_option('slider_select') == "slidorion"){ ?>
 
    <script type="text/javascript">
    jQuery(window).load(function() {
        jQuery('#slidorion').slidorion({effect: 'slideRandom', speed: <?php echo of_get_option('sliderspeed_text'); ?>, controlNav: true,directionNav: false,controlNavClass: 'nav'});
    });
    </script>
<?php } ?>
<script>
		$(document).ready(function() {
		
		    $('.social').hover(
			    function() {
			        $(this).find('.shutter').stop(true, true).animate({
					    bottom: '-36px'
					 },
					 {
					 	duration: 300,
					 	easing: 'easeOutBounce'
					 });
			    },
			    function () {
				    $(this).find('.shutter').stop(true, true).animate({
					    bottom: 0
					 },
					 {
					 	duration: 300,
					 	easing: 'easeOutBounce'
					 });
				}
		    );
		    
		});
	</script>
<script type="text/javascript">
	/* <![CDATA[ */
		jQuery().ready(function() {

	jQuery('#navmenu').prepend('<div id="menu-icon"><?php _e('Menu', 'ducj') ?></div>');
	jQuery("#menu-icon").on("click", function(){
		jQuery("#navmenu .menu").slideToggle();
		jQuery(this).toggleClass("menu_active");
	});

		});
	/* ]]> */
	</script>
    
<script type="text/javascript" charset="utf-8">
  
    $(window).load(function() {
      $('.tf-header-slider').flexslider({
        animation: "fade",
        maxItems: 11,
        controlNav: true
      });
    });
    
    $(window).load(function() {
      $('.tf-work-carousel').flexslider({
        animation: "slade",
        animationLoop: false,
        itemWidth: 280,
        itemMargin: 30,
        move: 1,
        start: function(slider){
          $('body').removeClass('loading');
        }
      });
    });
    
    $(window).load(function() {
      $('.tf-footer-carousel').flexslider({
        animation: "slide",
        animationLoop: true,
        itemWidth: 140,
        itemMargin: 15,
        minItems: 1,
        maxItems: 6,
        move:1
      });
    });
    
    jQuery(document).ready(function($) {
				$('#work-carousel').carouFredSel({
					next : "#work-carousel-next",
					prev : "#work-carousel-prev",
					auto: false,
					circular: false,
					infinite: true,	
					width: '100%',		
					scroll: {
						items : 1					
					}		
				});
			});
  </script> 
 
<?php } 



 


/*  Related posts
/* ------------------------------------ */
	function ducj_related_posts() {
		wp_reset_postdata();
		global $post;

		// Define shared post arguments
		$args = array(
			'no_found_rows'				=> TRUE,
			'update_post_meta_cache'	=> FALSE,
			'update_post_term_cache'	=> FALSE,
			'ignore_sticky_posts'		=> 1,
			'orderby'					=> 'rand',
			'post__not_in'				=> array($post->ID),
			'posts_per_page'			=> 3
		);
		// Related by categories
		if (of_get_option('related_posts')== 'categories'  ) {
			
			$cats = get_post_meta($post->ID, 'related-cat', TRUE);
			
			if ( !$cats ) {
				$cats = wp_get_post_categories($post->ID, array('fields'=>'ids'));
				$args['category__in'] = $cats;
			} else {
				$args['cat'] = $cats;
			}
		}
		// Related by tags
		if ( of_get_option('related_posts') == "tags" ) {
		
			$tags = get_post_meta($post->ID, 'related-tag', TRUE);
			
			if ( !$tags ) {
				$tags = wp_get_post_tags($post->ID, array('fields'=>'ids'));
				$args['tag__in'] = $tags;
			} else {
				$args['tag_slug__in'] = explode(',', $tags);
			}
			if ( !$tags ) { $break = TRUE; }
		}
		
		$query = !isset($break)?new WP_Query($args):new WP_Query;
		return $query;
	}
	







//ducj get the first image of the post Function
function ducj_get_images($overrides = '', $exclude_thumbnail = false)
{
    return get_posts(wp_parse_args($overrides, array(
        'numberposts' => -1,
        'post_parent' => get_the_ID(),
        'post_type' => 'attachment',
        'post_mime_type' => 'image',
        'order' => 'ASC',
        'exclude' => $exclude_thumbnail ? array(get_post_thumbnail_id()) : array(),
        'orderby' => 'menu_order ID'
    )));
}


//**************SLIDER SETUP******************//
// Change what's hidden by default
add_filter('default_hidden_meta_boxes', 'osiris_hidden_meta_boxes', 10, 2);
function osiris_hidden_meta_boxes($hidden, $screen) {
	if ( 'post' == $screen->base || 'page' == $screen->base || 'slider' == $screen->base  )
		$hidden = array('slugdiv', 'trackbacksdiv', 'commentstatusdiv', 'commentsdiv', 'authordiv', 'revisionsdiv');
		// removed 'postcustom',
	return $hidden;
}


add_action('init', 'osiris_slider_register');
 
function osiris_slider_register() {
 
	$labels = array(
		'name' => __('Slider', 'osiris'),
		'singular_name' => __('Slider Item', 'osiris'),
		'add_new' => __('Add New', 'osiris'),
		'add_new_item' => __('Add New Slide', 'osiris'),
		'edit_item' => __('Edit Slides', 'osiris'),
		'new_item' => __('New Slider', 'osiris'),
		'view_item' => __('View Sliders', 'osiris'),
		'search_items' => __('Search Sliders', 'osiris'),
		'menu_icon' => get_stylesheet_directory_uri() . 'images/article16.png',
		'not_found' =>  __('Nothing found', 'osiris'),
		'not_found_in_trash' => __('Nothing found in Trash', 'osiris'),
		'parent_item_colon' => ''
	);
 
	$args = array(
		'labels' => $labels,
		'public' => true,
		'exclude_from_search' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		'menu_icon' => get_stylesheet_directory_uri() . '/images/slides.png',
		'rewrite' => false,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => null,
		'supports' => array('title','excerpt','thumbnail'),
		'register_meta_box_cb' => 'osiris_add_meta'
	  ); 
 
	register_post_type( 'slider' , $args );
}
//Slider Link Meta Box
add_action("admin_init", "osiris_add_meta");
 
function osiris_add_meta(){
  add_meta_box("osiris_credits_meta", "Link", "osiris_credits_meta", "slider", "normal", "low");
}
 

function osiris_credits_meta( $post ) {

  // Use nonce for verification
  $osirisdata = get_post_meta($post->ID, 'osiris_slide_link', TRUE);
  wp_nonce_field( 'osiris_meta_box_nonce', 'meta_box_nonce' ); 

  // The actual fields for data entry
  echo '<input type="text" id="osiris_sldurl" name="osiris_sldurl" value="'.$osirisdata.'" size="75" />';
}

//Save Slider Link Value
add_action('save_post', 'osiris_save_details');
function osiris_save_details($post_id){
  if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
      return;

if( !isset( $_POST['meta_box_nonce'] ) || !wp_verify_nonce( $_POST['meta_box_nonce'], 'osiris_meta_box_nonce' ) ) return; 

  if ( !current_user_can( 'edit_post', $post_id ) )
        return;

$osirisdata = esc_url( $_POST['osiris_sldurl'] );
update_post_meta($post_id, 'osiris_slide_link', $osirisdata);
return $osirisdata;  
}



add_action('do_meta_boxes', 'osiris_slider_image_box');

function osiris_slider_image_box() {
	remove_meta_box( 'postimagediv', 'slider', 'side' );
	add_meta_box('postimagediv', __('Slide Image', 'osiris'), 'post_thumbnail_meta_box', 'slider', 'normal', 'high');
}



//ADD FULL WIDTH BODY CLASS
add_filter( 'body_class', 'ducj_fullwdth_body_class');
function ducj_fullwdth_body_class( $classes ) {
     if(of_get_option('nosidebar_checkbox') == "1")
          $classes[] = 'ducj_fullwdth_body';
     return $classes;
}

//Custom Excerpt Length
function ducj_excerptlength_teaser($length) {
    return 30;
}
function ducj_excerptlength_index($length) {
    return 12;
}
function ducj_excerptmore($more) {
    return '...';
}

function ducj_excerpt($length_callback='', $more_callback='') {
    global $post;
    if(function_exists($length_callback)){
        add_filter('excerpt_length', $length_callback);
    }
    if(function_exists($more_callback)){
        add_filter('excerpt_more', $more_callback);
    }
    $output = get_the_excerpt();
    $output = apply_filters('wptexturize', $output);
    $output = apply_filters('convert_chars', $output);
    $output = '<p>'.$output.'</p>';
    echo $output;
}


/*-----------------------------------------------------------------------------------*/
/* Display <title> tag
/*-----------------------------------------------------------------------------------*/

// filter function for wp_title
function ducj_filter_wp_title( $old_title, $sep, $sep_location ){
 
    // add padding to the sep
    $ssep = ' ' . $sep . ' ';
     
    // find the type of index page this is
    if( is_category() ) $insert = $ssep . __('Category','ducj');
    elseif( is_tag() ) $insert = $ssep . __('Tag','ducj');
    elseif( is_author() ) $insert = $ssep . __('Author','ducj');
    elseif( is_year() || is_month() || is_day() ) $insert = $ssep . __('Archives','ducj');
    elseif( is_home() ) $insert = $ssep . get_bloginfo('description');
    else $insert = NULL;
     
    // get the page number we're on (index)
    if( get_query_var( 'paged' ) )
    $num = $ssep . __('Page ','ducj') . get_query_var( 'paged' );
     
    // get the page number we're on (multipage post)
    elseif( get_query_var( 'page' ) )
    $num = $ssep . __('Page ','ducj') . get_query_var( 'page' );
     
    // else
    else $num = NULL;

     
    // concoct and return new title
    return get_bloginfo( 'name' ) . $insert . $old_title . $num;
}

add_filter( 'wp_title', 'ducj_filter_wp_title', 10, 3 );

function ducj_rss_title($title) {
    /* need to fix our add a | blog name to wp_title */
    $ft = str_replace(' | ','',$title);
    return str_replace(get_bloginfo('name'),'',$ft);
}
add_filter('get_wp_title_rss', 'ducj_rss_title',10,1);

	

	
/**
 * Add default options and show Options Panel after activate
 */
if (is_admin() && isset($_GET['activated'] ) && $pagenow == "themes.php" ) {
	
	//Do redirect

	wp_redirect( admin_url( 'admin.php?page=options-framework' ) ); exit;
	
}





//SIDEBAR
function ducj_widgets_init(){
	register_sidebar(array(
	'name'          => __('Right Sidebar', 'ducj'),
	'id'            => 'sidebar',
	'description'   => __('Right Sidebar', 'ducj'),
	'before_widget' => '<div id="%1$s" class="widget %2$s"><div class="widget_wrap">',
	'after_widget'  => '</div></div>',
	'before_title'  => '<h3 class="widgettitle">',
	'after_title'   => '</h3>'
	));
	
	register_sidebar(array(
	'name'          => __('Footer Widgets', 'ducj'),
	'id'            => 'foot_sidebar',
	'description'   => __('Widget Area for the Footer', 'ducj'),
	'before_widget' => '<div id="%1$s" class="widget %2$s"><div class="widget_wrap">',
	'after_widget'  => '</div></div>',
	'before_title'  => '<h3 class="widgettitle">',
	'after_title'   => '</h3>'
	));



 
	
	
}

add_action( 'widgets_init', 'ducj_widgets_init' );








//**************ducj SETUP******************//
function ducj_setup() {
//Custom Background
add_theme_support( 'custom-background', array(
	'default-color' => '',
	'default-image' => get_template_directory_uri() . ''
) );

add_theme_support('automatic-feed-links');

//Post Thumbnail	
   add_theme_support( 'post-thumbnails' );
   
   
//Register Menus
	register_nav_menus( array(
		'primary' => __( 'Primary Navigation(Header)', 'ducj' ),
		
	) );

 // Enables post and comment RSS feed links to head
    add_theme_support('automatic-feed-links');


// Localisation Support
    load_theme_textdomain('ducj', get_template_directory() . '/languages');
	
	 $locale = get_locale();
	$locale_file = get_template_directory() . "/languages/$locale.php";
	if ( is_readable( $locale_file ) )
		require_once( $locale_file );
        
    
/* 
 * Loads the Options Panel
 */
	/* Set the file path based on whether we're in a child theme or parent theme */

	if ( get_stylesheet_directory() == get_template_directory_uri() ) {
		define('OPTIONS_FRAMEWORK_URL', get_template_directory() . '/admin/');
		define('OPTIONS_FRAMEWORK_DIRECTORY', get_template_directory_uri() . '/admin/');
	} else {
		define('OPTIONS_FRAMEWORK_URL', get_stylesheet_directory() . '/admin/');
		define('OPTIONS_FRAMEWORK_DIRECTORY', get_stylesheet_directory_uri() . '/admin/');
	}

require_once (OPTIONS_FRAMEWORK_URL . 'options-framework.php');

include(get_template_directory() . '/lib/widgets.php');



}
add_action( 'after_setup_theme', 'ducj_setup' );



//Adding New Gallery Post Type/////////



$gallery_labels = array(
    'name' => _x('Gallery', 'post type general name'),
    'singular_name' => _x('Gallery', 'post type singular name'),
    'add_new' => _x('Add New', 'gallery'),
    'add_new_item' => __("Add New Gallery"),
    'edit_item' => __("Edit Gallery"),
    'new_item' => __("New Gallery"),
    'view_item' => __("View Gallery"),
    'search_items' => __("Search Gallery"),
    'not_found' =>  __('No galleries found'),
    'not_found_in_trash' => __('No galleries found in Trash'), 
    'parent_item_colon' => ''
        
);
$gallery_args = array(
    'labels' => $gallery_labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true, 
    'query_var' => true,
    'rewrite' => true,
    'hierarchical' => false,
    'menu_position' => null,
    'capability_type' => 'post',
    'supports' => array('title', 'excerpt', 'editor', 'thumbnail'),
    'menu_icon' => 'dashicons-format-image',
); 


register_post_type('gallery', $gallery_args);

add_action( 'init', 'jss_create_gallery_taxonomies', 0);
 
//**************Gallery Taxonomy**************/////// 

function jss_create_gallery_taxonomies(){
    register_taxonomy(
        'phototype', 'gallery', 
        array(
            'hierarchical'=> true, 
            'label' => 'Photo Types',
            'singular_label' => 'Photo Type',
            'rewrite' => true
        )
    );    
}
//**************Gallery Taxonomy**************/////// 


//----------------------------------------------
//--------------------------admin custom columns
//----------------------------------------------
//admin_init
add_action('manage_posts_custom_column', 'jss_custom_columns');
add_filter('manage_edit-gallery_columns', 'jss_add_new_gallery_columns');
 
function jss_add_new_gallery_columns( $columns ){
    $columns = array(
        'cb'                =>        '<input type="checkbox">',
        'jss_post_thumb'    =>        'Thumbnail',
        'title'                =>        'Photo Title',
        'phototype'            =>        'Photo Type',
        'author'            =>        'Author',
        'date'                =>        'Date'
        
    );
    return $columns;
}
 

function jss_custom_columns( $column ){
    global $post;
    
    switch ($column) {
        case 'jss_post_thumb' : echo the_post_thumbnail('admin-list-thumb'); break;
        case 'description' : the_excerpt(); break;
        case 'phototype' : echo get_the_term_list( $post->ID, 'phototype', '', ', ',''); break;
    }
}
 
//add thumbnail images to column
add_filter('manage_posts_columns', 'jss_add_post_thumbnail_column', 5);
add_filter('manage_pages_columns', 'jss_add_post_thumbnail_column', 5);
add_filter('manage_custom_post_columns', 'jss_add_post_thumbnail_column', 5);
 
// Add the column
function jss_add_post_thumbnail_column($cols){
    $cols['jss_post_thumb'] = __('Thumbnail');
    return $cols;
}
 
function jss_display_post_thumbnail_column($col, $id){
  switch($col){
    case 'jss_post_thumb':
      if( function_exists('the_post_thumbnail') )
        echo the_post_thumbnail( 'admin-list-thumb' );
      else
        echo 'Not supported in this theme';
      break;
  }
}

//----------------------------------------------
//--------------add theme support for thumbnails
//----------------------------------------------
if ( function_exists( 'add_theme_support')){
    add_theme_support( 'post-thumbnails' );
}
add_image_size( 'admin-list-thumb', 80, 80, true); //admin thumbnail preview
add_image_size( 'album-grid', 450, 450, true );


//End - //admin_init

//End - Adding New Gallery Post Type/////////


//gallery function

function jss_generate_tag_cloud( $tags, $args = '' ) {
    global $wp_rewrite;
 
    //don't touch these defaults or the sky will fall
    $defaults = array( 
        'smallest' => 8, 'largest' => 22, 'unit' => 'pt', 'number' => 0,
        'format' => 'flat', 'separator' => "\n", 'orderby' => 'name', 'order' => 'ASC',
        'topic_count_text_callback' => 'default_topic_count_text',
        'topic_count_scale_callback' => 'default_topic_count_scale', 'filter' => 1
    );
    
    //determine if the variable is null
    if ( !isset( $args['topic_count_text_callback'] ) && isset( $args['single_text'] ) && isset( $args['multiple_text'] ) ) {
        //var_export 
        $body = 'return sprintf (
            _n(' . var_export($args['single_text'], true) . ', ' . var_export($args['multiple_text'], true) . ', $count), number_format_i18n( $count ));';
        //create_function
        $args['topic_count_text_callback'] = create_function('$count', $body);
    }
    
    //parse arguments from above
    $args = wp_parse_args( $args, $defaults );
    
    //extract
    extract( $args );
    
    //check to see if they are empty and stop
    if ( empty( $tags ) )
        return;
    
    //apply the sort filter    
    $tags_sorted = apply_filters( 'tag_cloud_sort', $tags, $args );
    
    //check to see if the tags have been pre-sorted
    if ( $tags_sorted != $tags  ) { // the tags have been sorted by a plugin
        $tags = $tags_sorted;
        unset($tags_sorted);
    } else {
        if ( 'RAND' == $order ) {
            shuffle($tags);
        } else {
            // SQL cannot save you
            if ( 'name' == $orderby )
                uasort( $tags, create_function('$a, $b', 'return strnatcasecmp($a->name, $b->name);') );
            else
                uasort( $tags, create_function('$a, $b', 'return ($a->count > $b->count);') );
 
            if ( 'DESC' == $order )
                $tags = array_reverse( $tags, true );
        }
    }
    //check number and slice array
    if ( $number > 0 )
        $tags = array_slice($tags, 0, $number);
 
    //set array
    $counts = array();
    
    //set array for alt tag
    $real_counts = array();
    
    foreach ( (array) $tags as $key => $tag ) {
        $real_counts[ $key ] = $tag->count;
        $counts[ $key ] = $topic_count_scale_callback($tag->count);
    }
    
    //determine min coutn
    $min_count = min( $counts );
    
    //default wordpress sizing
    $spread = max( $counts ) - $min_count;
    if ( $spread <= 0 )
        $spread = 1;
    $font_spread = $largest - $smallest;
    if ( $font_spread < 0 )
        $font_spread = 1;
    $font_step = $font_spread / $spread;
 
    $a = array();
    
    //iterate thought the array
    foreach ( $tags as $key => $tag ) {
        $count = $counts[ $key ];
        $real_count = $real_counts[ $key ];
        $tag_link = '#' != $tag->link ? esc_url( $tag->link ) : '#';
        $tag_id = isset($tags[ $key ]->id) ? $tags[ $key ]->id : $key;
        $tag_name = $tags[ $key ]->name;
 
        //If you want to do some custom stuff, do it here like we did
        //call_user_func
        $a[] = "<a href='#filter' class='tag-link-$tag_id' 
        data-option-value='.$tag_name' 
        title='" . esc_attr( call_user_func( $topic_count_text_callback, $real_count ) ) . "'>$tag_name</a>"; //background-color is added for validation purposes.
    }
    
    //set new format
    switch ( $format ) :
    case 'array' :
        $return =& $a;
        break;
    case 'list' :
        //create our own setup of how it will display and add all
        $return = "<ul id='filters' class='option-set' data-option-key='filter'>\n\t
        <li><a href='filter' data-option-value='*' class='selected'>All</a></li>
        <li>";
        //join
        $return .= join( "</li>\n\t<li>", $a );
        $return .= "</li>\n</ul>\n";
        break;
    default :
        //return
        $return = join( $separator, $a );
        break;
    endswitch; 
        //create new filter hook so we can do this
        return apply_filters( 'jss_generate_tag_cloud', $return, $tags, $args );
}
 
//----------------------------------------------
//---------------------custom tag cloud function
//----------------------------------------------
 
//the function below is very similar to 'wp_tag_cloud()' currently located in: 'wp-includes/category-template.php'
function jss_tag_cloud( $args = '' ) {
    //set some default
    $defaults = array( 
        'format' => 'list', //display as list
        'taxonomy' => 'phototype', //our custom post type taxonomy
        'hide_empty' => 'true',
        'echo' => true, //touch this and it all blows up
        'link' => 'view'
    );
     
    //use wp_parse to merge the argus and default values
    $args = wp_parse_args( $args, $defaults );
    
    //go fetch the terms of our custom taxonomy. query by descending and order by most posts
    $tags = get_terms( $args['taxonomy'], array_merge( $args, array( 'orderby' => 'count', 'order' => 'DESC' ) ) );
     
    //if there are no tags then end function
    if ( empty( $tags ))
        return;
     
    //set the minimum number of posts the tag must have to display (change to whatever)
    $min_num = 1;
     
    //logic to display tag or not based on post count
    foreach($tags as $key => $tag)
        {   
            //if the post container lest than the min_num variable set above
            if($tag->count < $min_num)
            {
                //unset it and destroy part of the array
                unset($tags[$key]);
            }
        }
     
    foreach ( $tags as $key => $tag ) {
            if ( 'edit' == $args['link'] )
     
                //display the link to edit the tag, if the user is logged in and has rights
                $link = get_edit_tag_link( $tag -> term_id, $args['taxonomy'] );
            else
                //get the permalink for the taxonomy
                $link = get_term_link( intval($tag -> term_id), $args['taxonomy'] );
     
            //check if there is an error
            if ( is_wp_error( $link ) )
                return false;
     
        $tags[ $key ] -> link = $link;
        $tags[ $key ] -> id = $tag -> term_id;
    }
    //generate our tag cloud
    $return = jss_generate_tag_cloud( $tags, $args ); // here is where whe list what we are sorting
     
    //create a new filter hook
    $return = apply_filters( 'jss_tag_cloud', $return, $args );
     
    if ( 'array' == $args['format'] || empty($args['echo']) )
        return $return;
     
    echo $return;
}
//Hooks a function to a specific filter action.
//hook function to filter
add_filter('wp_tag_cloud', 'jss_tag_cloud');


//----------------------------------------------
//-------------------------get CPT taxonomy name
//----------------------------------------------
function jss_taxonomy_name(){
     global $post;
 
    //get terms for CPT
    $terms = get_the_terms( $post->ID , 'phototype' ); 
                //iterate through array
                foreach ( $terms as $termphoto ) { 
                    //echo taxonomy name as class
                    echo ' '.$termphoto->name; 
                } 
}



//----------------------------------------------
//------------------------------------enqueue js
//----------------------------------------------

/*
function jss_load_scripts(){
 
    //deregister for google jQuery cdn
    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', "http" . ( $_SERVER['SERVER_PORT'] == 443 ? "s" : "" ) . "://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js",  array(), false, true );
    wp_enqueue_script( 'jquery' );
    
    
}
//set it off
add_action( 'wp_enqueue_scripts', 'jss_load_scripts' );


*/

//To Be Edited Later

function wpa61143_enqueue_scripts() {
    if( is_page( 'gallery' ) ) :
    //wp_enqueue_script('ducj_js',get_template_directory_uri().'/other2.js');

    
    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', "http" . ( $_SERVER['SERVER_PORT'] == 443 ? "s" : "" ) . "://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js",  array(), false, true );
    wp_enqueue_script( 'jquery' );
	

    //register fancybox. change this to your local file.
    wp_deregister_script( 'fancybox' );
    wp_register_script( 'fancybox', "http" . ( $_SERVER['SERVER_PORT'] == 443 ? "s" : "" ) . "://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.4/jquery.fancybox.pack.js",  array(), false, true );
    wp_enqueue_script( 'fancybox' );

    //register isotope. change this to your local file.
    wp_deregister_script( 'isotope' );
    wp_register_script( 'isotope', "http" . ( $_SERVER['SERVER_PORT'] == 443 ? "s" : "" ) . "://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/1.5.25/jquery.isotope.min.js",  array(), false, true );
    wp_enqueue_script( 'isotope' );

    //general
    wp_register_script( 'general', get_template_directory_uri() . '/js/general.js',  array(), false, true );
    wp_enqueue_script( 'general' );

    endif;
}    

add_action( 'wp_enqueue_scripts', 'wpa61143_enqueue_scripts' );




//////////////////////////////////////////////Testimonial custom post type//////////////////////////////////////////////






?>