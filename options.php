<?php
/**
 * A unique identifier is defined to store the options in the database and reference them from the theme.
 * By default it uses the theme name, in lowercase and without spaces, but this can be changed if needed.
 * If the identifier changes, it'll appear as if the options have been reset.
 * 
 */

function optionsframework_option_name() {

	// This gets the theme name from the stylesheet
	$themename = get_option( 'stylesheet' );
	$themename = preg_replace("/\W/", "_", strtolower($themename) );

	$optionsframework_settings = get_option( 'optionsframework' );
	$optionsframework_settings['id'] = $themename;
	update_option( 'optionsframework', $optionsframework_settings );
}

/**
 * Defines an array of options that will be used to generate the settings page and be saved in the database.
 * When creating the "id" fields, make sure to use all lowercase and no spaces.
 *  
 */

function optionsframework_options() {
	
	// Test data
$number_array = array("1" => "One","2" => "Two","3" => "Three","4" => "Four","5" => "Five", "6" => "Six","7" => "Seven","8" => "Eight","9" => "Nine","10" => "Ten");
$numberfront_array = array("1" => "One","2" => "Two","3" => "Three","4" => "Four","5" => "Five", "6" => "Six","7" => "Seven","8" => "Eight","9" => "Nine","10" => "Ten","11" => "Eleven", "12" => "Twelve");

	
	// Fonts Array	
	$fonts_array = array(
	"Lora"=>"Lora",
	"Raleway"=>"Raleway","Yanone Kaffeesatz"=>"Yanone+Kaffeesatz","Allerta"=>"Allerta","Droid Sans"=>"Droid+Sans","Englebert"=>"Englebert",
	"Inika"=>"Inika","Wellfleet"=>"Wellfleet","Andika"=>"Andika","Atomic Age"=>"Atomic+Age","Bad Script"=>"Bad+Script","Cinzel"=>"Cinzel","Delius Swash Caps"=>"Delius+Swash+Caps","Loved by the King"=>"Loved+by+the+King","Lustria"=>"Lustria","Marck Script"=>"Marck+Script","Nova Slim"=>"Nova+Slim","Play"=>"Play","cinzel"=>"Cinzel","Roboto Slab"=>"Roboto+Slab","Signika Negative"=>"Signika+Negative","Sonsie One"=>"Sonsie+One","Signika Negative"=>"Signika+Negative","Sonsie One"=>"sonsie+One","Tangerine"=>"Tangerine","Vibur"=>"Vibur",
	);
	
	// Fontssize Array
	
	$size_array = array("8px"=>"8px","9px"=>"9px","10px"=>"10px","12px"=>"12px","12px"=>"12px","14px"=>"14px","16px"=>"16px","18px"=>"18px","22px"=>"22px","24px"=>"24px","26px"=>"26px","28px"=>"28px","30px"=>"30px","36px"=>"36px","38px"=>"38px","40px"=>"40px","42px"=>"42px","44px"=>"44px","46px"=>"46px","48px"=>"48px","52px"=>"52px","60px"=>"60px","70px"=>"70px","75px"=>"75px","80px"=>"80px");
	
	// Typography Options
	$typography_options = array(
		'sizes' => array( '6','12','14','16','20' ),
		'faces' => array( 'Helvetica Neue' => 'Helvetica Neue','Arial' => 'Arial' ),
		'styles' => array( 'normal' => 'Normal','bold' => 'Bold' ),
		'color' => false
	);

	

	
	
	
	// Test data
		$slider_array = array("nivo" => "Nivo Slider","slidorion"=> "slidorion slider","noslider" => "No Slider");
		
	// slider effcet 
	$effcet_array= array("sliceDown"=>"sliceDown","sliceDownLeft"=>"sliceDownLeft","sliceUp"=>"sliceUp","sliceUpLeft"=>"sliceUpLeft","sliceUpDown"=>"sliceUpDown","fold"=>"fold","fade"=>"fade","random"=>"random","slideInRight"=>"slideInRight","slideInLeft"=>"slideInLeft","boxRandom"=>"boxRandom","boxRain"=>"boxRain","boxRainReverse"=>"boxRainReverse","boxRainGrow"=>"boxRainGrow","boxRainGrowReverse"=>"boxRainGrowReverse");
	 	
		
		
		// Test data
		$head_array = array("head1" => "Head1","head2"=> "Head2","head3"=>"Head3","head4"=>"Head4");
	
	
		// Test data
		$related_array = array("categories" => "Related by categories","tags"=> "Related by tags");
	
	// Multicheck Defaults
	$multicheck_defaults = array("one" => "1","five" => "1");
	
	// Background Defaults
	
	$background_defaults = array('color' => '', 'image' => '', 'repeat' => 'repeat','position' => 'top center','attachment'=>'scroll');
	
	// Editor Defaults
		$wp_editor_settings = array(
		'wpautop' => true, // Default
		'textarea_rows' => 5,
		'media_buttons' => 'true',
		'tinymce' => array( 'plugins' => 'wordpress' )
	);
	
	
	// Pull all the categories into an array
	$options_categories = array();  
	$options_categories_obj = get_categories();
	foreach ($options_categories_obj as $category) {
    	$options_categories[$category->cat_ID] = $category->cat_name;
	}
	
	// Pull all the pages into an array
	$options_pages = array();  
	$options_pages_obj = get_pages('sort_column=post_parent,menu_order');
	$options_pages[''] = 'Select a page:';
	foreach ($options_pages_obj as $page) {
    	$options_pages[$page->ID] = $page->post_title;
	}
		
	// If using image radio buttons, define a directory path
	$imagepath =  get_template_directory_uri() . '/admin/images/';
	
	
	
		
	
	
	
	$options = array();
	
	
	
	
	
	$options[] = array( "name" => __('Front Page', 'ducj'),
						"type" => "heading");
	
	
						

							
			$options[] = array( "name" => __('Front Page Layout', 'ducj'),
						"desc" => "Select a front page layout",
						"id" => "layout1_images","layout2_images",
						"std" => "layout1",
						"type" => "images",
						"options" => array(
							'layout1' => $imagepath.'layout1.png',
							'layout2' => $imagepath.'layout2.png',
							'layout3' => $imagepath.'layout3.png',
							'layout4' => $imagepath.'layout4.png',
							'layout5' => $imagepath.'layout5.png',
							
							));
							
		
			
		
	$options[] = array( "name" => __('Select Header', 'ducj'),
						"desc" => "Select a header layout",
						"id" => "head_select",
						"std" => "head1",
						"type" => "select",
						"class" => "mini", //mini, tiny, small
						"options" => $head_array);
						
	   $options[] = array( "name" => __('Call us number and Address', 'ducj'),
						"desc" => "",
						"id" => "call_num",
						"std" => "<b>Call us number:</b>  +00 123 456 789",
						"type" => "editor",	
						'settings' => $wp_editor_settings);	
						
$options[] = array( "name" => __('Custom logo image', 'ducj'),
						"desc" => __('You can upload custom image for your website logo (optional).', 'ducj'),
						"id" => "ducj_logo_image",
						"type" => "upload");
						
	
	$options[] = array(     'name' => __('Upload a Favicon', 'options_framework_theme'),     
                        'desc' => __('Upload a favicon on your theme, size must be 16px by 16px', 'osiris'),     
						'id' => 'favicon_uploader',     
						'type' => 'upload');
	
	
	
		
			
		$options[] = array( "name" => __('Enable Home Page callout section', 'ducj'),
						"desc" => "Enable the Home Page callout section. You ca only use options below when this option is tick.",
						"id" => "callout_enable",
						"std" => "1",
						"type" => "checkbox");				
						
		 $options[] = array( "name" => __('Home Page Callout section 1', 'ducj'),
						"id" => "ducj_call1",
                 
						"std" => "<h5>Lorem ipsum dolor sit amet, consectetur dol adipiscing elit. Nam nec rhoncus risus. In ultrices lacinia ipsum, posuere faucibus velit bibe Nam nec rhoncus. </h5>",
						"type" => "editor", 
						
						'settings' => $wp_editor_settings);
	
		
	
	     $options[] = array( "name" => __('Callout section Link', 'ducj'),
						"desc" => "",
						"id" => "call1_link",
						"std" => "",
						"type" => "text");	
			$options[] = array( "name" => __('Callout section Link name', 'ducj'),
						"desc" => "",
						"id" => "call1_linkname",
						"std" => "Download",
						"class" => "mini",
						"type" => "text");	
						
						
		$options[] = array( "name" => __('Callout section background color', 'ducj'),
						"desc" => "Change Callout section background color ",
						"id" => "call1_colorpicker",
						"std" => "rgb(0, 0, 43)",
						"type" => "color");
	
	$options[] = array( "name" => __('Enable Home Page welcome section', 'ducj'),
						"desc" => "Enable the Home Page welcome section. You ca only use options below when this option is tick.",
						"id" => "welcome_enable",
						"std" => "1",
						"type" => "checkbox");
											
							
						
	 $options[] = array( "name" => __('Home Page welcome section', 'ducj'),
						"id" => "ducj_welcome",
                       
						"std" => "<h1>Welcome!</h1><h2>You need to configure your Home Page! </h2>Please visit Theme Options Page",
						"type" => "editor", 
						
						'settings' => $wp_editor_settings);
		
		$options[] = array( "name" => __('Wellcome section Link', 'ducj'),
						"desc" => "",
						"id" => "call_link",
						"std" => "",
						"type" => "text");	
						
	$options[] = array( "name" => __('Home Page welcome section background color', 'ducj'),
						"desc" => "Change Home Page welcome section background color ",
						"id" => "callcolor_colorpicker",
						"std" => "rgb(51, 51, 51)",
						"type" => "color");	
						
	$options[] = array(
         'name' => __('Latest Blog Title', 'ducj'),
		'desc' => __('Title For the Latest Blog', 'ducj'),
		'id' => 'latest_blog',
		'std' => 'Latest Blog',
		'type' => 'text');
				
						
	$options[] = array( "name" => __('Enable Latest Posts', 'ducj'),
						"desc" => "Enable the posts under the blocks on homepage. You ca only use options below when this option is tick.",
						"id" => "latstpst_checkbox",
						"std" => "1",
						"type" => "checkbox");
						
	$options[] = array( "name" => __('Show Posts from a certain Category', 'ducj'),
						"desc" => "Show posts from certain category on frontpage. Select the category below.",
						"id" => "frontcat_checkbox",
						"std" => "0",
						"type" => "checkbox");
						
						
	$options[] = array( "name" => __('Front Page Posts Category', 'ducj'),
						"desc" => "Select a Category For Front Page.",
						"id" => "front_cat",
						"type" => "select",
						"class" => "mini",
						"options" => $options_categories);						
						
	
	
	$options[] = array( "name" => __('Number of Front page Posts', 'ducj'),
						"desc" => "Select the Number of Posts.",
						"id" => "frontnum_select",
						"std" => "4",
						"type" => "select",
						"class" => "mini", //mini, tiny, small
						"options" => $numberfront_array);	
	
	$options[] = array( "name" => __('Footer Content', 'ducj'),
						"desc" => "Footer Text.",
						"id" => "footer_textarea",
						"std" => "",
						"type" => "textarea"); 	

     //Color & Font
		
	$options[] = array( "name" => __('Color & Font', 'ducj'),
						"type" => "heading");
						
						
			
						
	$options[] = array( "name" => __('Select Font', 'ducj'),
						"desc" => "",
						"id" => "font_select",
						"std" => "raleway",
						"type" => "select",
						"class" => "mini",
					    "options" => $fonts_array);
						
	
	
						
	$options[] = array( "name" => __('secondary background color', 'ducj'),
						"desc" => "Change secondary background color of theme",
						"id" => "bg_colorpicker",
						"std" => "#fff",
						"type" => "color");
	
	$options[] = array( "name" => __('Header color', 'ducj'),
						"desc" => "Change header background color",
						"id" => "header_colorpicker",
						"std" => "#333333",
						"type" => "color");
						
	$options[] = array( 
						"desc" => "Change header text color(titel and Tagline)",
						"id" => "headertext_colorpicker",
						"std" => "#fff",
						"type" => "color");
						
	$options[] = array( 
						"desc" => "Change Site titel font size",
						"id" => "fontsize_select",
						"std" => "30px",
						"type" => "text",
						"class" => "mini"
					 );
	
	$options[] = array( "name" => __('menu', 'ducj'),
						"desc" => "Change header Menu and footer copyright background color",
						"id" => "menu_colorpicker",
						"std" => "#1c1c1e",
						"type" => "color");
						
	$options[] = array( 
						"desc" => "Change header Menu and footer copyright text color",
						"id" => "menutext_colorpicker",
						"std" => "#fff",
						"type" => "color");
	
	$options[] = array( "name" => __('Title Color', 'ducj'),
						"desc" => "Change all element titel color",
						"id" => "title_colorpicker",
						"std" => "#e54222",
						"type" => "color");
						
	
						
	 $options[] = array( "name" => __('Hover Color', 'ducj'),
						"desc" => "Change all element hover color",
						"id" => "hover_colorpicker",
						"std" => "#ff4533",
						"type" => "color");
				
$options[] = array( "name" => __('Page Header background', 'ducj'),
						"desc" => "Change all Page Header color",
						"id" => "pagehead_colorpicker",
						"std" => "#fff",
						"type" => "color");
						


$options[] = array( 
						"desc" => "Change all Page Header text color",
						"id" => "pageheadtext_colorpicker",
						"std" => "#e54222",
						"type" => "color");
$options[] = array( 
						"desc" => "Change Site titel font size",
						"id" => " headerfs_select",
						"std" => "30px",
						"type" => "text",
						"class" => "mini"
					   );
						
	
						
	
						
	$options[] = array( "name" => __('All page text color', 'ducj'),
						"desc" => "Change All page text color",
						"id" => "text_colorpicker",
						"std" => "#1c1c1e",
						"type" => "color");
						
	$options[] = array( "name" => __('change footer wedgets background color', 'ducj'),
						"desc" => "change footer wedgets background color",
						"id" => "wedgets_colorpicker",
						"std" => "rgb(55, 55, 55)",
						"type" => "color");	
						
	$options[] = array( "name" => __('change footer text wedgets color', 'ducj'),
						"desc" => "change footer wedgets color",
						"id" => "wedtext_colorpicker",
						"std" => "rgb(55, 55, 55)",
						"type" => "color");	
						
	
		
	$options[] = array( "name" => __('Service block and wellcome block ', 'ducj'),
						"desc" => "Change Service block and wellcome block background color",
						"id" => "serv_colorpicker",
						"std" => "#fff",
						"type" => "color");
						
						
	$options[] = array( 
						"desc" => "Change Service block and wellcome block Text color",
						"id" => "welltext_colorpicker",
						"std" => "#333333",
						"type" => "color");					
						
	$options[] = array( 
						"desc" => "Change Service block Logo color",
						"id" => "serlogo_colorpicker",
						"std" => "#333333",
						"type" => "color");								
							
		
$options[] = array( "name" => __('Services Bloks', 'ducj'),
						"type" => "heading");		
		
$options[] = array( "name" => __('Enable Blocks', 'ducj'),
						"desc" => "Enable the homepage blocks.",
						"id" => "blocks_checkbox",
						"std" => "1",
						"type" => "checkbox");
						
    $options[] = array( "name" => __('Block 1 Logo', 'ducj'),
						"desc" => 'Icon name, for example: fa-heart
List of all avaialable icons and their names can be found at <a target="_blank" style=" color:#10a7d1;" href="http://fontawesome.io/cheatsheet/">FontAwesome</a>.',
						"id" => "block1_logo",
						"std" => "fa-heart",
						"type" => "text");
						
	$options[] = array( "name" => __('Block 1 Heading', 'ducj'),
						"desc" => "",
						"id" => "block1_text",
						"std" => "We Work Efficiently",
						"type" => "text");
							
	$options[] = array( "name" => __('Block 1 Text', 'ducj'),
						"desc" => "",
						"id" => "block1_textarea",
						"std" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam nec rhoncus risus. In ultrices lacinia ipsum, posuere faucibus velit bibendum ac.",
						"type" => "textarea"); 
						
	$options[] = array( "name" => __('Block 1 Link', 'ducj'),
						"desc" => "",
						"id" => "block1_link",
						"std" => "http://wordpress.org/",
						"type" => "text");
						
						
			
	
	  $options[] = array( "name" => __('Block 1 Logo', 'ducj'),
						"desc" => 'Icon name, for example:  fa-heart
List of all avaialable icons and their names can be found at <a target="_blank" style=" color:#10a7d1;" href="http://fontawesome.io/cheatsheet/">FontAwesome</a>.',
						"id" => "block2_logo",
						"std" => " fa-volume-up",
						"type" => "text");					
						
	$options[] = array( "name" => __('Block 2 Heading', 'ducj'),
						"desc" => "",
						"id" => "block2_text",
						"std" => "24/7 Live Support",
						"type" => "text");
							
	$options[] = array( "name" => __('Block 2 Text', 'ducj'),
						"desc" => "",
						"id" => "block2_textarea",
						"std" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam nec rhoncus risus. In ultrices lacinia ipsum, posuere faucibus velit bibendum ac. Sed ultrices leo.",
						"type" => "textarea"); 
						
	$options[] = array( "name" => __('Block 2 Link', 'ducj'),
						"desc" => "",
						"id" => "block2_link",
						"std" => "",
						"type" => "text");
						
						
	$options[] = array( "name" => __('Block 3 Logo', 'ducj'),
						"desc" => 'Icon name, for example:  fa-heart
List of all avaialable icons and their names can be found at <a target="_blank" style=" color:#10a7d1;" href="http://fontawesome.io/cheatsheet/">FontAwesome</a>.',
						"id" => "block3_logo",
						"std" => "fa-fighter-jet",
						"type" => "text");		

	$options[] = array( "name" => __('Block 3 Heading', 'ducj'),
						"desc" => "",
						"id" => "block3_text",
						"std" => "Confide",
						"type" => "text");
							
	$options[] = array( "name" => __('Block 3 Text', 'ducj'),
						"desc" => "",
						"id" => "block3_textarea",
						"std" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam nec rhoncus risus. In ultrices lacinia ipsum, posuere faucibus velit bibendum ac. ",
						"type" => "textarea");
						
	$options[] = array( "name" => __('Block 3 Link', 'ducj'),
						"desc" => "",
						"id" => "block3_link",
						"std" => "",
						"type" => "text");
	
	
	$options[] = array( "name" => __('Block 1 Logo', 'ducj'),
						"desc" => 'Icon name, for example: fa-heart
List of all avaialable icons and their names can be found at <a target="_blank" style=" color:#10a7d1;" href="http://fontawesome.io/cheatsheet/">FontAwesome</a>.',
						"id" => "block4_logo",
						"std" => "fa-cogs",
						"type" => "text");		

	$options[] = array( "name" => __('Block 4 Heading', 'ducj'),
						"desc" => "",
						"id" => "block4_text",
						"std" => "Gurantee Like No Other",
						"type" => "text");
							
	$options[] = array( "name" => __('Block 4 Text', 'ducj'),
						"desc" => "",
						"id" => "block4_textarea",
						"std" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam nec rhoncus risus. In ultrices lacinia ipsum.",
						"type" => "textarea"); 
						
	$options[] = array( "name" => __('Block 4 Link', 'ducj'),
						"desc" => "",
						"id" => "block4_link",
						"std" => "",
						"type" => "text");		
		
		
//Recent work

$options[] = array( "name" => __('Recent work', 'ducj'),
					"desc" => "More recent work option in Pro version",
						"type" => "heading");
						


$options[] = array( "name" => __('Enable Recent work', 'ducj'),
						"desc" => "Enable the homepage recent work.image size 300X222 px(More recent work post option in Pro version)",
						"id" => "recentwork_checkbox",
						"std" => "1",
						
						"type" => "checkbox");
						
$options[] = array( "name" => __('Link Name  (Read More) ', 'ducj'),
						"desc" => "Read More",
						"id" => "read_more",
						"std" => "Read More",
						"class" => "mini",
						"type" => "text");
						
$options[] = array(
         'name' => __('Title', 'ducj'),
		'desc' => __('Title of the recent work slider', 'ducj'),
		'id' => 'recent_work',
		'std' => 'Our Work',
		'type' => 'text');

$options[] = array(
		'name' => __('Image 1', 'ducj'),
		'desc' => __('First image', 'ducj'),
		'id' => 'recent1',
		'class' => '',
		'std' =>get_template_directory_uri().'/images/demo/bg2.png',
		'type' => 'upload');
	
	$options[] = array(
		'desc' => __('Title', 'options_framework_theme'),
		'id' => 'recenttitle1',
		'std' => 'Beautiful Photo',
		'type' => 'text');
	
	$options[] = array(
		'desc' => __('Description or Tagline', 'options_framework_theme'),
		'id' => 'recentdesc1',
		'std' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy.',
		'type' => 'textarea');			
		
	$options[] = array(
		'desc' => __('Url', 'options_framework_theme'),
		'id' => 'recenturl1',
		'std' => '',
		'type' => 'text');	


$options[] = array(
		'name' => __('Image 2', 'ducj'),
		'desc' => __('2nd image', 'ducj'),
		'id' => 'recent2',
		'class' => '',
		'std' =>get_template_directory_uri().'/images/demo/bg4.png',
		'type' => 'upload');
	
	$options[] = array(
		'desc' => __('Title', 'options_framework_theme'),
		'id' => 'recenttitle2',
		'std' => 'Beautiful Photo',
		'type' => 'text');
	
	$options[] = array(
		'desc' => __('Description or Tagline', 'options_framework_theme'),
		'id' => 'recentdesc2',
		'std' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy.',
		'type' => 'textarea');			
		
	$options[] = array(
		'desc' => __('Url', 'options_framework_theme'),
		'id' => 'recenturl2',
		'std' => '',
		'type' => 'text');	
		
	$options[] = array(
		'name' => __('Image 3', 'ducj'),
		'desc' => __('3rd image', 'ducj'),
		'id' => 'recent3',
		'class' => '',
		'std' =>get_template_directory_uri().'/images/demo/bg5.png',
		'type' => 'upload');
	
	$options[] = array(
		'desc' => __('Title', 'options_framework_theme'),
		'id' => 'recenttitle3',
		'std' => 'Beautiful Photo',
		'type' => 'text');
	
	$options[] = array(
		'desc' => __('Description or Tagline', 'options_framework_theme'),
		'id' => 'recentdesc3',
		'std' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy.',
		'type' => 'textarea');			
		
	$options[] = array(
		'desc' => __('Url', 'options_framework_theme'),
		'id' => 'recenturl3',
		'std' => '',
		'type' => 'text');		
		
		
	
	
	
	$options[] = array(
		'name' => __('Image 4', 'ducj'),
		'desc' => __('4th image', 'ducj'),
		'id' => 'recent4',
		'std' => get_template_directory_uri().'/images/demo/bg5.png',
		'class' => '',
		'type' => 'upload');
	
	$options[] = array(
		'desc' => __('Title', 'options_framework_theme'),
		'id' => 'recenttitle4',
		'std' => 'Beautiful Photo',
		'type' => 'text');
	
	$options[] = array(
		'desc' => __('Description or Tagline', 'options_framework_theme'),
		'id' => 'recentdesc4',
		'std' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy.',
		'type' => 'textarea');			
		
	$options[] = array(
		'desc' => __('Url', 'options_framework_theme'),
		'id' => 'recenturl4',
		'std' => '',
		'type' => 'text');		
		
		
	
	$options[] = array(
		'name' => __('Image 5', 'ducj'),
		'desc' => __('5th image', 'ducj'),
		'id' => 'recent5',
		'class' => '',
		'type' => 'upload');
	
	$options[] = array(
		'desc' => __('Title', 'options_framework_theme'),
		'id' => 'recenttitle5',
		'std' => 'Beautiful Photo',
		'type' => 'text');
	
	$options[] = array(
		'desc' => __('Description or Tagline', 'options_framework_theme'),
		'id' => 'recentdesc5',
		'std' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy.',
		'type' => 'textarea');			
		
	$options[] = array(
		'desc' => __('Url', 'options_framework_theme'),
		'id' => 'recenturl5',
		'std' => '',
		'type' => 'text');	
	
	
	$options[] = array(
		'name' => __('Image 6', 'ducj'),
		'desc' => __('6th image', 'ducj'),
		'id' => 'recent6',
		'class' => '',
		'type' => 'upload');
	
	$options[] = array(
		'desc' => __('Title', 'options_framework_theme'),
		'id' => 'recenttitle6',
		'std' => 'Beautiful Photo',
		'type' => 'text');
	
	$options[] = array(
		'desc' => __('Description or Tagline', 'options_framework_theme'),
		'id' => 'recentdesc6',
		'std' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy.',
		'type' => 'textarea');			
		
	$options[] = array(
		'desc' => __('Url', 'options_framework_theme'),
		'id' => 'recenturl6',
		'std' => '',
		'type' => 'text');	
		
		
	$options[] = array(
		'name' => __('Image 7', 'ducj'),
		'desc' => __('7th image', 'ducj'),
		'id' => 'recent7',
		'class' => '',
		'type' => 'upload');
	
	$options[] = array(
		'desc' => __('Title', 'options_framework_theme'),
		'id' => 'recenttitle7',
		'std' => 'Beautiful Photo',
		'type' => 'text');
	
	$options[] = array(
		'desc' => __('Description or Tagline', 'options_framework_theme'),
		'id' => 'recentdesc7',
		'std' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy.',
		'type' => 'textarea');			
		
	$options[] = array(
		'desc' => __('Url', 'options_framework_theme'),
		'id' => 'recenturl7',
		'std' => '',
		'type' => 'text');	
		
	
	$options[] = array(
		'name' => __('Image 8', 'ducj'),
		'desc' => __('8th image', 'ducj'),
		'id' => 'recent8',
		'class' => '',
		'type' => 'upload');
	
	$options[] = array(
		'desc' => __('Title', 'options_framework_theme'),
		'id' => 'recenttitle8',
		'std' => 'Beautiful Photo',
		'type' => 'text');
	
	$options[] = array(
		'desc' => __('Description or Tagline', 'options_framework_theme'),
		'id' => 'recentdesc8',
		'std' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy.',
		'type' => 'textarea');			
		
	$options[] = array(
		'desc' => __('Url', 'options_framework_theme'),
		'id' => 'recenturl8',
		'std' => '',
		'type' => 'text');	

   
	$options[] = array(
		'name' => __('Image 9', 'ducj'),
		'desc' => __('9th image', 'ducj'),
		'id' => 'recent9',
		'class' => '',
		'type' => 'upload');
	
	$options[] = array(
		'desc' => __('Title', 'options_framework_theme'),
		'id' => 'recenttitle9',
		'std' => 'Beautiful Photo',
		'type' => 'text');
	
	$options[] = array(
		'desc' => __('Description or Tagline', 'options_framework_theme'),
		'id' => 'recentdesc9',
		'std' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy.',
		'type' => 'textarea');			
		
	$options[] = array(
		'desc' => __('Url', 'options_framework_theme'),
		'id' => 'recenturl9',
		'std' => '',
		'type' => 'text');	
		
		
//Our Client

$options[] = array( "name" => __('Our Client', 'ducj'),
						"type" => "heading");

$options[] = array( "name" => __('Our Client', 'ducj'),
	                   'desc' => __('Image size must be 223px*113px. If you leave this field empty, the our client section will not be displayed.', 'ducj'),
					   'type' => 'info'
						); 	
						
	$options[] = array(
         'name' => __('Our Client Title', 'ducj'),
		'desc' => __('Title of the Our Client', 'ducj'),
		'id' => 'our_client',
		'std' => 'Our Client',
		'type' => 'text');
							
	$options[] = array(
		'name' => __('Client One', 'ducj'),
		'desc' => __('Upload image for client one ', 'ducj'),
		'id' => 'client1',
		'class' => '',
		'std' =>get_template_directory_uri().'/images/demo/logo1.png',
		'type' => 'upload');
	
	$options[] = array(
		'desc' => __('Url', 'options_framework_theme'),
		'id' => 'clienturl1',
		'std' => '',
		'type' => 'text');
		
	$options[] = array(
		'name' => __('Client 2', 'ducj'),
		'desc' => __('Upload image for client 2 ', 'ducj'),
		'id' => 'client2',
		'class' => '',
		'std' =>get_template_directory_uri().'/images/demo/logo2.png',
		'type' => 'upload');
	
	$options[] = array(
		'desc' => __('Url', 'options_framework_theme'),
		'id' => 'clienturl2',
		'std' => '',
		
		'type' => 'text');
	
	$options[] = array(
		'name' => __('Client 3', 'ducj'),
		'desc' => __('Upload image for client 3 ', 'ducj'),
		'id' => 'client3',
		'class' => '',
		'std' =>get_template_directory_uri().'/images/demo/logo3.png',
		'type' => 'upload');
	
	$options[] = array(
		'desc' => __('Url', 'options_framework_theme'),
		'id' => 'clienturl3',
		'std' => '',
		'type' => 'text');
	
	$options[] = array(
		'name' => __('Client 4', 'ducj'),
		'desc' => __('Upload image for client 4 ', 'ducj'),
		'id' => 'client4',
		'class' => '',
		'std' =>get_template_directory_uri().'/images/demo/logo4.png',
		'type' => 'upload');
	
	$options[] = array(
		'desc' => __('Url', 'options_framework_theme'),
		'id' => 'clienturl4',
		'std' => '',
		'type' => 'text');		
		
											
	
					
	//slider		
						
	$options[] = array( "name" => __('Slider', 'ducj'),
						"type" => "heading");
						
	$options[] = array( "name" => __('Select Slider', 'ducj'),
						"desc" => "",
						"id" => "slider_select",
						"std" => "nivo",
						"type" => "select",
						"class" => "mini", //mini, tiny, small
						"options" => $slider_array);
	
	$options[] = array( "name" => __('Select Slider effcet (nivo)', 'ducj'),
						"desc" => "",
						"id" => "effcet_select",
						"std" => "random",
						"type" => "select",
						"class" => "mini", //mini, tiny, small
						"options" => 	$effcet_array);
		
	$options[] = array( "name" => __('Slider Content Opacity control (Nivo Slider) ', 'ducj'),
						"desc" => "Opacity",
						"id" => "opacity_colorpicker",
						"std" => "0.5",
						"class" => "mini",
						"type" => "text");
						
	$options[] = array( 
						"desc" => "Slider titel font size",
						"id" => "titels_colorpicker",
						"std" => "42px",
						"class" => "mini",
						"type" => "text");	
	$options[] = array( 
						"desc" => "Slider caption font size",
						"id" => "caption_colorpicker",
						"std" => "16px",
						"class" => "mini",
						"type" => "text");	
						
						
	$options[] = array( "name" => __('Slider Speed', 'ducj'),
						"desc" => "milliseconds",
						"id" => "sliderspeed_text",
						"std" => "4000",
						"class" => "mini",
						"type" => "text");	
						
$options[] = array( "name" => __('Number of Slides', 'ducj'),
						"desc" => "",
						"id" => "number_select",
						"std" => "5",
						"type" => "select",
						"class" => "mini", //mini, tiny, small
						"options" => $number_array);						
						
	$options[] = array( "name" => __('Slider Content', 'ducj'),
						"desc" => "Show Slider text",
						"id" => "sldrtxt_checkbox",
						"std" => "1",
						"type" => "checkbox");	
						
	
		
	

			
	
			
	$options[] = array( "name" => __('Social', 'ducj'),
						"type" => "heading");						
						
						
	$options[] = array( "name" => __('Facebook', 'ducj'),
						"desc" => "Your Facebook url",
						"id" => "fbsoc_text",
						"std" => "#",
						"type" => "text");
						
	$options[] = array( "name" => __('Twitter', 'ducj'),
						"desc" => "Your Twitter url",
						"id" => "ttsoc_text",
						"std" => "#",
						"type" => "text");
						
	$options[] = array( "name" => __('Google Plus', 'ducj'),
						"desc" => "Your Google Plus url",
						"id" => "gpsoc_text",
						"std" => "#",
						"type" => "text");
						
	$options[] = array( "name" => __('Youtube', 'ducj'),
						"desc" => "Your Youtube url",
						"id" => "ytbsoc_text",
						"std" => "#",
						"type" => "text");
						
	
						
	$options[] = array( "name" => __('Pinterest', 'ducj'),
						"desc" => "Your Pinterest url",
						"id" => "pinsoc_text",
						"std" => "#",
						"type" => "text");
						
	$options[] = array( "name" => __('vimeo', 'ducj'),
						"desc" => "Your vimeo url",
						"id" => "vimsoc_text",
						"std" => "#",
						"type" => "text");
	$options[] = array( "name" => __('LinkedIn', 'ducj'),
						"desc" => "Your LinkedIn url",
						"id" => "linsoc_text",
						"std" => "#",
						"type" => "text");
						
	$options[] = array( "name" => __('flickr', 'ducj'),
						"desc" => "Your flickr url",
						"id" => "flisoc_text",
						"std" => "#",
						"type" => "text");
						
	$options[] = array( "name" => __('Rss', 'ducj'),
						"desc" => "Your RSS url",
						"id" => "rsssoc_text",
						"std" => "#",
						"type" => "text");
	
	
		
		
		//misceleneous				

	$options[] = array( "name" => __('Miscelleneous', 'ducj'),
						"type" => "heading");
						
		$options[] = array( "name" => __('Page Header ', 'ducj'),
						"desc" => "Enable Page Header ",
						"id" => "pagehead_checkbox",
						"std" => "1",
						"type" => "checkbox");					
						
	
	$options[] = array( "name" => __('Related Post', 'ducj'),
						"desc" => "Show related articles",
						"id" => "rtl_checkbox",
						"std" => "1",
						"type" => "checkbox");
							
				
											
	$options[] = array( 
						"desc" => "Shows randomized related articles below the post",
						"id" => "related_posts",
						"std" => "categories",
						"type" => "select",
						"class" => "mini", //mini, tiny, small
						"options" => $related_array);					
						
	$options[] = array( "name" => __('Right Sidebar', 'ducj'),
						"desc" => "Remove Right sidebar from all the pages and make the site full width.",
						"id" => "nosidebar_checkbox",
						"std" => "0",
						"type" => "checkbox");
						
						
	$options[] = array( "name" => __('comments template', 'ducj'),
						"desc" => "Show comments template in post",
						"id" => "comments_checkbox",
						"std" => "1",
						"type" => "checkbox");							
  					
	
						
						
	$options[] = array( "name" => __('Post Author Name', 'ducj'),
						"desc" => "Hide Post Author Name",
						"id" => "dissauth_checkbox",
						"std" => "0",
						"type" => "checkbox");
						
	$options[] = array( "name" => __('Category & Tags', 'ducj'),
						"desc" => "Hide Post Categories and Tags",
						"id" => "disscats_checkbox",
						"std" => "0",
						"type" => "checkbox");
	
	
	
	
	$options[] = array( "name" => __('Google Analytics', 'ducj'),
						"desc" => "Add Google Analytics code",
						"id" => "google_textarea",
						"std" => "",
						"type" => "textarea"); 	
						
$options[] = array( "name" => __('Documentation', 'ducj'),
						"type" => "heading");
						
						
$options[] = array( "name" => __('Our Client', 'ducj'),
	                   'desc' => __('<b>About the theme</b> <br><p style="text-align:justify;font-size:14px;color:#000 !important;">ducj is a Simple, Clean and Responsive Retina Ready WordPress Theme which adapts automatically to your tablets and mobile devices. theme with 2 home page layouts,10 social icons,4 widget ,Slider,3 page templates – Full width page, 4 google fonts, font-awesome service icon,Upload logo option,The theme is translation ready and fully translated into all language. ducj is suitable for any types of website – corporate, portfolio, business, blog.</p></br></br>For Documentation <a target="_blank" href="http://bit.ly/18LKXw7">Download This PDF.</a> <h4 style="text-align:center;color: #000;>About Developer</h4>
<p style="color: #000;> This Theme is designed and devloped by <a target="_blank" style=" color:#10a7d1;" href="http://www.PunalThemes.com/"><span>Imon Themes</span></a><br /></p> 

<p style="color:#000;font-size: 18px;">1. Setting up the front  page</p><br>
<p style="color:#000;font-size: 14px;">				  
When you  select &ldquo;Your Latest Posts&rdquo; from Settings&gt; Reading you will be able to  display 7 frontpage elements on your site&rsquo;s frontpage. They are: <br>
<br>
i. Slider<br>
ii. Blocks<br>
iii. Welcome  Text<br>
iv. Frontpage  Posts<br>
v. Call to  Action<br>
vi. Recent work<br>
vii.Our Client</p>
<br />
<br />
</p>
<p><p  style="color:#000;font-size: 16px;">i. Setting up the slider</p>
  From <strong>Slider => Add New</strong>  For each slide you should set: 
<p><img src="http://i.imgur.com/pHBVhNQ.png" alt="Create New Slide" width="637" height="493"></p>
  <br>
			
	<p style="color:#000;font-size: 16px;"> a.<strong>Image:</strong> Select/Upload Slide image by  clicking the Set featured image. If you are using the &ldquo;Full Width&rdquo; mode,  make sure the slider images have at least 1600px of width. If set to fixed, the  slider images should have minimum width of 1200px. If your images are bigger,  you can resize and crop them online using this application: <a target="_blank" href="http://pixlr.com/editor/">http://pixlr.com/editor/</a>. There&rsquo;s also a video tutorial on  youtube on how to resize and crop your images and save them: <a target="_blank" href="https://www.youtube.com/watch?v=WmFjvNlm1E4">https://www.youtube.com/watch?v=WmFjvNlm1E4</a><br>
  b. <strong>Title:</strong> Write a title of your  slide. This is optional; if you don&rsquo;t want to display the title of the slide  you should keep this empty.<br>
   c. <strong>Description:</strong> If you want to display a  little subtext under the title of the slide you should use this field. You can  use HTML tags.<br>
  d. <strong>Url:</strong> If you want your slide  image and title to contain a link, you should put it here.<br>	
  
   <p> 





'




, 'ducj'),
					   
					   'type' => 'info'
						); 							
	
	
	
													
	return $options;
}