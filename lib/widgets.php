<?php
/**
 * Include CSS
 */
 /*Load Other CSS files*/

function ducj_news_scroller_css() { 
if ( !is_admin() ) {	
wp_enqueue_style( 'ducj_news_scroller', get_template_directory_uri() . '/lib/assets/style.css' );

}  }
add_action('wp_enqueue_scripts', 'ducj_news_scroller_css');	

/*Load Java Scripts to header*/
function ducj_news_scroller_js() { 
if ( !is_admin() ) {
wp_enqueue_script('jquery');
wp_enqueue_script('ducj_news_scroller_js',get_template_directory_uri().'/lib/assets/flexslider.js');


}
}
add_action('wp_enqueue_scripts', 'ducj_news_scroller_js');

/**
 * Include JS
 */

function ducj_news_scroller_scripts() {
	wp_register_script( 'flexslider', get_template_directory_uri( 'theme-blvd-news-scroller/assets/flexslider.js', __FILE__ ), array('jquery'), '2.1' );
}
add_action( 'wp_enqueue_scripts', 'ducj_news_scroller_scripts', 11 ); // Priority 11, so this call to FlexSlider doesn't override ducj themes.

/**
 * Limit excerpt by number of words
 * 
 * @param $string string Excerpt to limit
 * @param $word_limit int Number of words to limit excerpt by
 */
 
function ducj_new_scroller_excerpt( $string, $word_limit ) {
	$words = explode(' ', $string, ($word_limit + 1));
	if(count($words) > $word_limit)
	array_pop($words);
	return implode(' ', $words).'...';
}

/**
 * ducj New Scroller Widget
 * 
 * @package ducj WordPress Framework
 * @author Jason Bobich
 */

class TB_Widget_News_Scroller extends WP_Widget {
	
	/* Constructor */
	
	function __construct() {
		$widget_ops = array(
			'classname' => 'tb-news_scroller_widget', 
			'description' => 'This will scroll through posts from a category.'
		);
		$control_ops = array(
			'width' => 400, 
			'height' => 350
		);
        $this->WP_Widget( 'ducj_news_scroller_widget', 'ducj News Scroller', $widget_ops, $control_ops );
	}
	
	/* Widget Options Form */
	
	function form($instance) {
		$defaults = array(
			'title' => '',
            'category' => '',
            'date' => 'show',
            'excerpt' => 'show',
            'image' => 'show',
            'excerpt_limit' => 20,
            'scroll_timeout' => 5,
            'scroll_direction' => 'vertical',
            'num' => 10,
            'height' => 300
        );        
        $instance = wp_parse_args( (array) $instance, $defaults );
        ?>
		<!-- Title -->
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', 'ducj_scroller'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($instance['title']); ?>" />
		</p>
		
		<!-- Category -->
		<p>
			<label for="<?php echo $this->get_field_id('category'); ?>"><?php _e( 'Category', 'ducj_scroller' ); ?> </label>
			<select class="widefat" id="<?php echo $this->get_field_id('category'); ?>" name="<?php echo $this->get_field_name('category'); ?>">
				<?php 
				$list = null;
				$categories = get_categories();
				$options = array( '' => __( 'All Categories', 'ducj_scroller' ) );
				foreach ( $categories as $category ) $options[$category->term_id] = $category->name;
				foreach ( $options as $id => $name ) {
					$selected = '';
					if($id == $instance['category']) $selected = 'selected="selected"';
					$list .= '<option '.$selected.' value="'.$id.'">'.$name.'</option>';
				}
				echo $list;
				?>
			</select>
		</p>
		
		<!-- Show/Hide Date -->
		<p>
			<label for="<?php echo $this->get_field_id('date'); ?>"><?php _e( 'Show post dates?', 'ducj_scroller' ); ?> </label>
			<select class="widefat" id="<?php echo $this->get_field_id('date'); ?>" name="<?php echo $this->get_field_name('date'); ?>">
				<?php 
				$list = null;
				$options = array( 'show', 'hide' );
				foreach ( $options as $option ) {
					$selected = "";
					if($option == $instance['date']) $selected = 'selected="selected"';
					$list .= "<option $selected value='$option'>$option</option>";

				}
				echo $list;
				?>
			</select>
		</p>
		
		<!-- Show/Hide Excerpt -->
		<p>
			<label for="<?php echo $this->get_field_id('excerpt'); ?>"><?php _e( 'Show post excerpts?', 'ducj_scroller' ); ?> </label>
			<select class="widefat" id="<?php echo $this->get_field_id('excerpt'); ?>" name="<?php echo $this->get_field_name('excerpt'); ?>">
				<?php 
				$list = null;
				$options = array( 'show', 'hide' );
				foreach ( $options as $option ) {
					$selected = "";
					if($option == $instance['excerpt']) $selected = 'selected="selected"';
					$list .= "<option $selected value='$option'>$option</option>";
				}
				echo $list;
				?>
			</select>
		</p>
		
		<!-- Show/Hide Featured Image -->
		<p>
			<label for="<?php echo $this->get_field_id('image'); ?>"><?php _e( 'Show featured images?', 'ducj_scroller' ); ?> </label>
			<select class="widefat" id="<?php echo $this->get_field_id('image'); ?>" name="<?php echo $this->get_field_name('image'); ?>">
				<?php 
				$list = null;
				$options = array( 'show', 'hide' );
				foreach ( $options as $option ) {
					$selected = "";
					if($option == $instance['image']) $selected = 'selected="selected"';
					$list .= "<option $selected value='$option'>$option</option>";
				}
				echo $list;
				?>
			</select>
		</p>
		
		<!-- Scroll Direction -->
		<p>
			<label for="<?php echo $this->get_field_id('scroll_direction'); ?>"><?php _e( 'How to transition?', 'ducj_scroller' ); ?> </label>
			<select class="widefat" id="<?php echo $this->get_field_id('scroll_direction'); ?>" name="<?php echo $this->get_field_name('scroll_direction'); ?>">
				<?php 
				$list = null;
				$options = array( 'vertical' => __('Scroll Vertical', 'ducj_scroller'), 'horizontal' => __('Scroll Horizontal', 'ducj_scroller'), 'fade' => __('Fade', 'ducj_scroller') );
				foreach ( $options as $key => $name ) {
					$selected = "";
					if($key == $instance['scroll_direction']) $selected = 'selected="selected"';
					$list .= "<option $selected value='$key'>$name</option>";
				}
				echo $list;
				?>
			</select>
		</p>
				
		<!-- Scroll Timeout -->
		<p>
			<label for="<?php echo $this->get_field_id('scroll_timeout'); ?>"><?php _e('Scroll Timeout', 'ducj_scroller'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('scroll_timeout'); ?>" name="<?php echo $this->get_field_name('scroll_timeout'); ?>" type="text" value="<?php echo esc_attr($instance['scroll_timeout']); ?>" />
			<span style="display:block;padding:5px 0" class="description"><?php _e( 'Enter in the number of seconds in between the posts scrolling. Set this number to 0 if you don\'t want the posts to auto scroll.', 'ducj_scroller' ); ?></span>
		</p>
		
		<!-- Excerpt Limit -->
		<p>
			<label for="<?php echo $this->get_field_id('excerpt_limit'); ?>"><?php _e('Excerpt Limit', 'ducj_scroller'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('excerpt_limit'); ?>" name="<?php echo $this->get_field_name('excerpt_limit'); ?>" type="text" value="<?php echo esc_attr($instance['excerpt_limit']); ?>" />
			<span style="display:block;padding:5px 0" class="description"><?php _e( 'If you have the excerpts set to show you can enter in a number of total <strong>words</strong> to automatically limit the excerpts by. Note that WordPress limits excerpts to 55 words by default. To allow excerpts to show normally, simply leave this blank.', 'ducj_scroller' ); ?></span>
		</p>
		
		<!-- Number of Posts -->
		<p>
			<label for="<?php echo $this->get_field_id('num'); ?>"><?php _e('Maximum Number of Posts', 'ducj_scroller'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('num'); ?>" name="<?php echo $this->get_field_name('num'); ?>" type="text" value="<?php echo esc_attr($instance['num']); ?>" />
			<span style="display:block;padding:5px 0" class="description"><?php _e( 'Enter in a maximum number of posts for the scroller. Leave blank to pull all posts from specified category.', 'ducj_scroller' ); ?></span>
		</p>
		
		<!-- Height -->
		<p>
			<label for="<?php echo $this->get_field_id('height'); ?>"><?php _e('Scroller Height', 'ducj_scroller'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('height'); ?>" name="<?php echo $this->get_field_name('height'); ?>" type="text" value="<?php echo esc_attr($instance['height']); ?>" />
			<span style="display:block;padding:5px 0" class="description"><?php _e( 'Enter in a number of pixels to make the height of each visible section. Depending on the different settings you\'ve chosen, your scroller will have varying amounts of content, however it\'s important that the each visible section have the same fixed height for the scroller to animate properly. So feel free to play with this number until it looks how you want.', 'ducj_scroller' ); ?></span>
		</p>
        <?php
	}
	
	/* Update Widget Settings */
	
	function update($new_instance, $old_instance) {
		$instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['category'] = strip_tags($new_instance['category']);
        $instance['date'] = strip_tags($new_instance['date']);
        $instance['excerpt'] = strip_tags($new_instance['excerpt']);
        $instance['image'] = strip_tags($new_instance['image']);
        $instance['excerpt_limit'] = strip_tags($new_instance['excerpt_limit']);
        $instance['scroll_timeout'] = strip_tags($new_instance['scroll_timeout']);
        $instance['scroll_direction'] = strip_tags($new_instance['scroll_direction']);
        $instance['num'] = strip_tags($new_instance['num']);
        $instance['height'] = strip_tags($new_instance['height']);
        return $instance;
	}
	
	/* Display Widget */
	
	function widget($args, $instance) {
		global $post;
		extract( $args );
		// Setup args
		$i = 1;
		$height = $instance['height'] ? $instance['height'] : 130;
		$excerpt_limit = $instance['excerpt_limit'] ? $instance['excerpt_limit'] : 55;
		$number_posts = $instance['num'] ? $instance['num'] : 10;
		$category = $instance['category'] ? $instance['category'] : '';
		$scroll_timeout = strval( $instance['scroll_timeout'] );
		$args = array(
			'category' => $category,
			'numberposts' => $number_posts,
		);
		$slide_offset = $instance['scroll_direction'] == 'fade' ? 1 : 2;
		// Get Posts
		$posts = get_posts( $args );
		$count = count($posts);
		// Start output
		echo $before_widget;
		$title = apply_filters( 'widget_title', $instance['title'] );
		if ( $title )
			echo $before_title . $title . $after_title;
		// Enqueue script
		wp_enqueue_script( 'flexslider' );
		?>
		<script type="text/javascript">
		jQuery(document).ready(function($) {
			$(window).load(function() {
				$('#<?php echo $widget_id; ?> .flexslider').flexslider({
					<?php if( $instance['scroll_direction'] == 'fade' ) : ?>
					animation: 'fade',
					<?php else : ?>
					animation: 'slide',
					slideDirection: '<?php echo $instance['scroll_direction']; ?>', // For FlexSlider v1.x
					direction: '<?php echo $instance['scroll_direction']; ?>', // For FlexSlider v2.x
					<?php endif; ?>
					controlsContainer: '#<?php echo $widget_id; ?> .scroller-nav',
					controlNav: false,
					animationDuration: 800,
					<?php if( $scroll_timeout == 0 ) : ?>
					slideshow: false,
					<?php else : ?>
					slideshow: true,
					slideshowSpeed: <?php echo $scroll_timeout;?>000,
					<?php endif; ?>
					start: function(slider){
						var num = <?php echo $slide_offset; ?>, // account for "clone" Flexslider adds (if not fade)
							date = slider.container.find('li:nth-child('+num+')').find('.scroller-date').text();
						slider.closest('.ducj-news-scroller').find('.scroller-nav span').text(date).fadeIn('fast');
						$('#<?php echo $widget_id; ?> .scroller-nav').fadeIn();
					},
					before: function(slider){
						slider.closest('.ducj-news-scroller').find('.scroller-nav span').slideUp();
					},
					after: function(slider){
						var num = slider.currentSlide+<?php echo $slide_offset; ?>, // account for "clone" slide plugin adds (if not fade)
							date = slider.container.find('li:nth-child('+num+')').find('.scroller-date').text();
						slider.closest('.ducj-news-scroller').find('.scroller-nav span').text(date).fadeIn('fast');
					}
				});
			});
		});
		</script>
		<div class="ducj-news-scroller flex-container">
			<div class="scroller-nav scroller-nav-<?php echo $instance['scroll_direction']; ?>"><span></span><!-- nav inserted here --></div>
			<div class="scroller-wrap">
				<div class="flexslider">
					<ul class="slides">
						<?php foreach( $posts as $post ) : ?>
							<?php setup_postdata($post); ?>
							<li class="scroller-post" style="height:<?php echo $height; ?>px;">
								<div class="scroller-header">
									<h4><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h4>
								</div><!-- .scroller-header (end) -->
								<?php if( $instance['image'] == 'show' ) : ?>
								    <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'grid_6' ); ?>
								    <?php if( $image ) : ?>
								        <div class="scroller-image">
									        <img src="<?php echo $image[0]; ?>" />
								        </div><!-- .scroller-image (end) -->
								    <?php endif; ?>
								<?php endif; ?>
								<?php if( $instance['excerpt'] == 'show' ) : ?>
									<div class="scroller-content">
										<p><?php echo ducj_new_scroller_excerpt( get_the_excerpt(), $excerpt_limit ); ?></p>
									</div><!-- .scroller-content (end) -->
								<?php endif; ?>
								<?php if( $instance['date'] == 'show' ) : ?>
									<span class="scroller-date"><?php the_time( get_option('date_format') ); ?></span>
								<?php endif; ?>
							</li>
							<?php $i++; ?>
						<?php endforeach; ?>
						<?php wp_reset_query(); ?>	
					</ul>
				</div><!-- .flexslider (end) -->
			</div><!-- .scroller-wrap (end) -->
		</div><!-- .ducj-news-scroller (end) -->
		<?php
		echo $after_widget;		
	}

}
add_action( 'widgets_init', create_function( '', 'register_widget("TB_Widget_News_Scroller");' ) );

/* ---------------------------- */
/* -------- Video like -------- */
/* ---------------------------- */
 
    $video_defaults = array(
        'title' => 'Video',
        'videos' => array(
			array(
				'title' => 'Amazing nature scenery', 
				'url' => 'http://www.youtube.com/watch?v=6v2L2UGZJAM', 
				'type' => 'youtube', 
				'id' => '6v2L2UGZJAM'
			)
		)
    );


class ducjVideoFeed extends WP_Widget 
{
    function __construct(){
        
        $widget_options = array('description' => 'Video Feed Box widget.' );
        $control_options = array();
        parent::__construct('ducjVideoFeed', '&raquo; ducj:Video Feed', $widget_options, $control_options);
    }

    function widget($args, $instance){
        extract( $args );
        $title = apply_filters('widget_title', $instance['title']);
        $videos = $instance['videos'];
   
        if(is_array($videos)) {
            ?>
			<?php echo $args['before_widget']?>
			<?php  if ( $title ) {  ?><?php echo $args['before_title']?><?php echo $title?><?php echo $args['after_title']?><?php } 
				foreach ($videos as $video) {?><div class='lz-sidebar-video'>
				<a href='<?php echo $video['url']; ?>' rel='nofollow' target='_blank'><?php echo $video['title']; ?></a>
			<?php
				switch( $video['type'] ) {
					case 'youtube':
					echo '<p style="text-align:center;display:block;overflow:hidden;clear:left"><a href="http://www.youtube.com/watch?v='.$video['id'].'" target="_blank" alt="'.$video['id'].'" src="youtube" class="video popup"><img src="http://img.youtube.com/vi/'.$video['id'].'/0.jpg" width="100%" /></a></p>';
				}?>
				</div>
				<?php }
            ?>
           <?php echo $args['after_widget']?>
            <?php
        }
    }

    function update($new_instance, $old_instance) 
    {				
    	$instance = $old_instance;
    	$instance['title'] = strip_tags($new_instance['title']);
		$instance['videos']=array();
		foreach ($new_instance['videos'] as $video) {
			if (preg_match('/youtube/', $video['url'])) {
				$video['type']='youtube';
				$video['id']=preg_replace('/(.*)(v=)(.*)/', '$3', $video['url']);
			}
			$instance['videos'][]=$video;
		}
        return $instance;
    }
    
     function form($instance){
		global $video_defaults;
		$instance = wp_parse_args( (array) $instance, $video_defaults );
        $get_videos = $instance['videos'];
		$get_this_id = preg_replace("/[^0-9]/", '', $this->get_field_id('this_id_videos'));
        $get_this_id = !$get_this_id ? 'this_id_videos___i__' : 'this_id_videos_' . $get_this_id;
        ?>
        
        <script type="text/javascript">
		
			jQuery('.video-ttl').live('focusin', function() {
				jQuery(this).parents('.video-element').addClass('active').siblings().removeClass('active');
				jQuery('.video-ttl').removeClass('field');
				jQuery(this).addClass('field');
			});
		
			jQuery('.add-video').die();
			jQuery('.add-video').live('click', function() {
				var new_video_id = 10000+Math.floor(Math.random()*100000);
				jQuery(this).before(
					jQuery('<div>', {
						'class':'video-element'
					}).append(
						jQuery('<input>', {
							'class':'video-ttl widefat',
							'type':'text',
							'value':'title',
							'name':'<?php echo $this->get_field_name('videos')?>['+new_video_id+'][title]'
						})
					).append(
						jQuery('<input>', {
							'class':'widefat',
							'type':'text',
							'value':'url',
							'name':'<?php echo $this->get_field_name('videos')?>['+new_video_id+'][url]'
						})
					).append(
						jQuery('<a>', {
							'class':'button delete_video'
						}).text('Delete')
					)
				);
				jQuery(this).prev('.video-element').find('.video-ttl').focus();
			});
			jQuery('.delete_video').live('click', function() {
				jQuery(this).parents('.video-element').remove();
			});
		</script>
		<style>
			.video-element.active {
				height:100px;
			}
			.video-element {
				border: 1px solid #DFDFDF;
				margin-bottom: 20px;
				padding: 10px;
				height:23px;
				line-height:23px;
				overflow:hidden;
			}
			.video-element input {
				margin-bottom:12px;
			}
			.video-ttl {
				cursor:pointer;
				border:none !important;
				background:none !important;
			}
			.video-ttl.field {
				cursor:text;
				border:1px solid #DFDFDF !important;
				background:#fff !important;
			}
			.add-video {
				cursor:pointer;
			}
		</style>
        <div style="margin-bottom: 20px;">
			<p><label for="<?php echo $this->get_field_id('title')?>">Title:</label><input class="widefat" id="<?php echo $this->get_field_id('title')?>" name="<?php echo $this->get_field_name('title')?>" type="text" value="<?php echo esc_attr($instance['title'])?>" /></p>
			
        </div>
		<div class="videos_<?php echo $get_this_id?>">
        <?php
            if(is_array($get_videos)) {
                foreach($get_videos as $video_id=>$video_source) {
                    ?>
                    <div class="tt-clearfix video-element">
							
						<input class='video-ttl widefat' name="<?php echo $this->get_field_name('videos')?>[<?php echo $video_id ?>][title]" type="text" value="<?php echo $video_source['title']?>" />
						<input class="widefat" name="<?php echo $this->get_field_name('videos')?>[<?php echo $video_id ?>][url]" type="text" value="<?php echo $video_source['url']?>" />
							
						<a class="button delete_video">Delete</a>
							
                    </div>
                    <?php
                }
            }
        ?>
					<div class="tt-clearfix video-element add-video">
							Add new...
                    </div>
		</div>
        <?php
    }
} 
add_action('widgets_init', create_function('', 'return register_widget("ducjVideoFeed");'));

/* ---------------------------- */
/* -------- Facebook like -------- */
/* ---------------------------- */

$fb_defaults = array(
			'title' => 'Facebook',
			'url' => 'http://www.facebook.com/PunalThemes',
			'width' => '240',
			'height' => '180',
			'colorscheme' => 'light',
			'show_faces' => 'true',
			'stream' => 'false',
			'header' => 'false',
			'border' => 'dbdbdb'
);

		
class ducjFacebook extends WP_Widget { 
	function __construct(){
        $widget_options = array('description' => 'Facebook Like Box social widget. Enables Facebook Page owners to attract and gain Likes from their own website.' );
        $control_options = array( 'width' => 440 );
		$this->WP_Widget('ducjFacebook', '&raquo; ducj:Facebook Like Box', $widget_options, $control_options);
    }

     function widget($args, $instance){
        global $wpdb;
        extract( $args );
        $title = apply_filters('widget_title', $instance['title']);
        $url = $instance['url'];
        $width = $instance['width'];
        $height = $instance['height'];
        $colorscheme = $instance['colorscheme'];
        $show_faces = $instance['show_faces'] == 'true' ? 'true' : 'false';
        $stream = $instance['stream'] == 'true' ? 'true' : 'false';
        $header = $instance['header'] == 'true' ? 'true' : 'false';
        $border = $instance['border'];
        ?>
       <?php echo $args['before_widget']?>
        <?php  if ( $title ) {  ?><?php echo $args['before_title']?><?php echo $title?><?php echo $args['after_title']?><?php }  ?>
            <div id="fb-root"></div>
            <script>(function(d, s, id) {
              var js, fjs = d.getElementsByTagName(s)[0];
              if (d.getElementById(id)) {return;}
              js = d.createElement(s); js.id = id;
              js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
              fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));</script>
            <div class="fb-like-box" data-href="<?php echo $url?>" data-width="<?php echo $width?>" data-height="<?php echo $height?>" data-colorscheme="<?php echo $colorscheme?>" data-show-faces="<?php echo $show_faces?>" data-stream="<?php echo $stream?>" data-header="<?php echo $header?>" data-border-color="<?php echo $border?>"></div>
            
          <?php echo $args['after_widget']?>
     <?php
    }


    function update($new_instance, $old_instance){
    	$instance = $old_instance;
    	$instance['title'] = strip_tags($new_instance['title']);
        $instance['url'] = strip_tags($new_instance['url']);
        $instance['width'] = strip_tags($new_instance['width']);
        $instance['height'] = strip_tags($new_instance['height']);
        $instance['colorscheme'] = strip_tags($new_instance['colorscheme']);
        $instance['show_faces'] = strip_tags($new_instance['show_faces']);
        $instance['stream'] = strip_tags($new_instance['stream']);
        $instance['header'] = strip_tags($new_instance['header']);
        $instance['border'] = strip_tags($new_instance['border']);
        return $instance;
    }
    
    function form($instance){
		global $fb_defaults;
        $instance=wp_parse_args( (array)$instance, $fb_defaults);
        ?>
        
            <div class="tt-widget">
                <table width="100%">
                    <tr>
                        <td class="tt-widget-label" width="30%"><label for="<?php echo $this->get_field_id('title')?>">Title:</label></td>
                        <td class="tt-widget-content" width="70%"><input class="widefat" id="<?php echo $this->get_field_id('title')?>" name="<?php echo $this->get_field_name('title')?>" type="text" value="<?php echo esc_attr($instance['title'])?>" /></td>
                    </tr>
                    
                    <tr>
                        <td class="tt-widget-label"><label for="<?php echo $this->get_field_id('url')?>">Facebook Page URL:</label></td>
                        <td class="tt-widget-content"><input class="widefat" id="<?php echo $this->get_field_id('url')?>" name="<?php echo $this->get_field_name('url')?>" type="text" value="<?php echo esc_attr($instance['url'])?>" /></td>
                    </tr>
                    
                    <tr>
                        <td class="tt-widget-label">Sizes:</td>
                        <td class="tt-widget-content">
                            Width: <input type="text" style="width: 50px;" name="<?php echo $this->get_field_name('width')?>" value="<?php echo esc_attr($instance['width'])?>" /> px. &nbsp; &nbsp;
                            Height: <input type="text" style="width: 50px;" name="<?php echo $this->get_field_name('height')?>" value="<?php echo esc_attr($instance['height'])?>" /> px.
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="tt-widget-label">Color Scheme:</td>
                        <td class="tt-widget-content">
                            <select name="<?php echo $this->get_field_name('colorscheme')?>">
                                <option value="light" <?php selected('light', $instance['colorscheme']); ?> >Light</option>
                                <option value="dark"  <?php selected('dark', $instance['colorscheme']); ?>>Dark</option>
                             </select>      
                             &nbsp; &nbsp; Border Color: <input type="text" style="width: 50px;" name="<?php echo $this->get_field_name('border')?>" value="<?php echo esc_attr($instance['border'])?>" /> <em>e.g: #ffffff</em>                      
                        </td>
                    </tr>

                    <tr>
                        <td class="tt-widget-label">Misc Options:</td>
                        <td class="tt-widget-content">
                            <input type="checkbox" name="<?php echo $this->get_field_name('show_faces')?>"  <?php checked('true', $instance['show_faces']); ?> value="true" />  Show faces
                            <br /><input type="checkbox" name="<?php echo $this->get_field_name('stream')?>"  <?php checked('true', $instance['stream']); ?> value="true" />  Show stream
                            <br /><input type="checkbox" name="<?php echo $this->get_field_name('header')?>"  <?php checked('true', $instance['header']); ?> value="true" />  Show  header
                        </td>
                    </tr>
                    
                </table>
            </div>
            
        <?php 
    }
} 
add_action('widgets_init', create_function('', 'return register_widget("ducjFacebook");'));

/* ---------------------------- */
/* -------- Popular Posts Widget -------- */
/* ---------------------------- */
add_action( 'widgets_init', 'ducj_pop_widgets' );

/*
 * Register widget.
 */
function ducj_pop_widgets() {
	register_widget( 'ducj_pop_Widget' );
}

/*
 * Widget class.
 */
class ducj_pop_widget extends WP_Widget {

	/* ---------------------------- */
	/* -------- Widget setup -------- */
	/* ---------------------------- */
	
	function ducj_pop_Widget() {
	
		/* Widget settings */
		$widget_ops = array( 'classname' => 'ducj_pop_widget', 'description' => __('A ducj widget that displays the most popular posts of the site, Based on the comments count ', 'ducj') );

		/* Widget control settings */
		//$control_ops = array( 'width' => 160, 'height' => 600, 'id_base' => 'thn_pop_widget' );

		/* Create the widget */
		$this->WP_Widget( 'ducj_pop_widget', __('Popular Posts Widget', 'ducj'), $widget_ops );
	}

	/* ---------------------------- */
	/* ------- Display Widget -------- */
	/* ---------------------------- */
	
	function widget( $args, $instance ) {
		extract( $args );

		/* Our variables from the widget settings. */
		$title = apply_filters('widget_title', $instance['title'] );
		$num = $instance['num'];

		/* Before widget (defined by themes). */
		echo $before_widget;

		/* Display the widget title if one was input (before and after defined by themes). */
		if ( $title )
			echo $before_title . $title . $after_title;
			
		/* Display a containing div */
		echo '<div class="thn_pop">';

		/* Display Posts */
		if ( $num )
		$popular = new WP_Query('ignore_sticky_posts=1&orderby=comment_count&posts_per_page=' . $num);
		
		echo '<ul>';
		while ($popular->have_posts()) : $popular->the_post();
		echo '<li>';
		echo '<a class="thn_wgt_thumb" href="' . get_permalink(get_the_ID()) . '" title="' . get_the_title() . '">';
		if ( has_post_thumbnail() ) :
		echo ''. the_post_thumbnail('thumbnail') . '';
		elseif($photo = ducj_get_images('numberposts=1', true)):
		echo ''. wp_get_attachment_image($photo[0]->ID , $size='thumbnail') . '';
		else :
		echo '<img src="'.get_template_directory_uri().'/images/blank_img2.png" alt="'.get_the_title().'" class="thumbnail"/>';
		endif;
		echo '</a>';
		echo '<div class="widget_content">';
		echo '<a class="thn_wgt_tt" href="' . get_permalink(get_the_ID()) . '" title="' . get_the_title() . '">' . get_the_title() . '</a><br />' ;
		echo ''. ducj_excerpt('ducj_excerptlength_index', 'ducj_excerptmore') . '';
		echo '</div>';
		echo '</li>';
    
		endwhile;
		
		echo '</ul>';
			
		echo '</div>';

		/* After widget (defined by themes). */
		echo $after_widget;
	}

	/* ---------------------------- */
	/* ------- Update Widget -------- */
	/* ---------------------------- */
	
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		/* Strip tags for title and name to remove HTML (important for text inputs). */
		$instance['title'] = strip_tags( $new_instance['title'] );

		/* No need to strip tags */
		$instance['num'] = $new_instance['num'];

		return $instance;
	}
	
	/* ---------------------------- */
	/* ------- Widget Settings ------- */
	/* ---------------------------- */
	
	/**
	 * Displays the widget settings controls on the widget panel.
	 * Make use of the get_field_id() and get_field_name() function
	 * when creating your form elements. This handles the confusing stuff.
	 */
	
	function form( $instance ) {
	
		/* Set up some default widget settings. */
		$defaults = array(
		'title' => '',
		'num' => 3,
		);
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<!-- Widget Title: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'ducj') ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" />
		</p>


		<!-- Number of Posts: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'num' ); ?>"><?php _e('Number of Posts:', 'ducj') ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'num' ); ?>" name="<?php echo $this->get_field_name( 'num' ); ?>" value="<?php echo $instance['num']; ?>" />
		</p>
		
		
	<?php
	}

}


/* ---------------------------- */
/* -------- Random Posts Widget -------- */
/* ---------------------------- */

add_action( 'widgets_init', 'ducj_rand_widgets' );

/*
 * Register widget.
 */
function ducj_rand_widgets() {
	register_widget( 'ducj_rand_Widget' );
}

/*
 * Widget class.
 */
class ducj_rand_widget extends WP_Widget {

	/* ---------------------------- */
	/* -------- Widget setup -------- */
	/* ---------------------------- */
	
	function ducj_rand_Widget() {
	
		/* Widget settings */
		$widget_ops = array( 'classname' => 'ducj_rand_widget', 'description' => __('A ducj widget that displays the random posts of the site', 'ducj') );

		/* Widget control settings */
		//$control_ops = array( 'width' => 160, 'height' => 600, 'id_base' => 'thn_rand_widget' );

		/* Create the widget */
		$this->WP_Widget( 'ducj_rand_widget', __('Random Posts Widget', 'ducj'), $widget_ops );
	}

	/* ---------------------------- */
	/* ------- Display Widget -------- */
	/* ---------------------------- */
	
	function widget( $args, $instance ) {
		extract( $args );

		/* Our variables from the widget settings. */
		$title = apply_filters('widget_title', $instance['title'] );
		$num = $instance['num'];

		/* Before widget (defined by themes). */
		echo $before_widget;

		/* Display the widget title if one was input (before and after defined by themes). */
		if ( $title )
			echo $before_title . $title . $after_title;
			
		/* Display a containing div */
		echo '<div class="thn_rand">';

		/* Display Posts */
		if ( $num )
		$popular = new WP_Query('ignore_sticky_posts=1&orderby=rand&posts_per_page=' . $num);
		
		echo '<ul>';
		while ($popular->have_posts()) : $popular->the_post();
		echo '<li>';
		echo '<a class="thn_wgt_thumb" href="' . get_permalink(get_the_ID()) . '" title="' . get_the_title() . '">';
		if ( has_post_thumbnail() ) :
		echo ''. the_post_thumbnail('thumbnail') . '';
		elseif($photo = ducj_get_images('numberposts=1', true)):
		echo ''. wp_get_attachment_image($photo[0]->ID , $size='thumbnail') . '';
		else :
		echo '<img src="'.get_template_directory_uri().'/images/blank_img2.png" alt="'.get_the_title().'" class="thumbnail"/>';
		endif;
		echo '</a>';
		echo '<div class="widget_content">';
		echo '<a class="thn_wgt_tt" href="' . get_permalink(get_the_ID()) . '" title="' . get_the_title() . '">' . get_the_title() . '</a><br />' ;
		echo ''. ducj_excerpt('ducj_excerptlength_index', 'ducj_excerptmore') . '';
		echo '</div>';
		echo '</li>';
    
		endwhile;
		
		echo '</ul>';
			
		echo '</div>';

		/* After widget (defined by themes). */
		echo $after_widget;
	}

	/* ---------------------------- */
	/* ------- Update Widget -------- */
	/* ---------------------------- */
	
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		/* Strip tags for title and name to remove HTML (important for text inputs). */
		$instance['title'] = strip_tags( $new_instance['title'] );

		/* No need to strip tags */
		$instance['num'] = $new_instance['num'];

		return $instance;
	}
	
	/* ---------------------------- */
	/* ------- Widget Settings ------- */
	/* ---------------------------- */
	
	/**
	 * Displays the widget settings controls on the widget panel.
	 * Make use of the get_field_id() and get_field_name() function
	 * when creating your form elements. This handles the confusing stuff.
	 */
	
	function form( $instance ) {
	
		/* Set up some default widget settings. */
		$defaults = array(
		'title' => '',
		'num' => 3,
		);
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<!-- Widget Title: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'ducj') ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" />
		</p>

		<!-- Number of Posts: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'num' ); ?>"><?php _e('Number of Posts:', 'ducj') ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'num' ); ?>" name="<?php echo $this->get_field_name( 'num' ); ?>" value="<?php echo $instance['num']; ?>" />
		</p>
		
		
	<?php
	}

}

/*
	/* ---------------------------- */
	/* -------- Featured Posts Widget -------- */
	/* ---------------------------- */
add_action( 'widgets_init', 'ducj_feat_widgets' );

/*
 * Register widget.
 */
function ducj_feat_widgets() {
	register_widget( 'ducj_feat_Widget' );
}

/*
 * Widget class.
 */
class ducj_feat_widget extends WP_Widget {

	/* ---------------------------- */
	/* -------- Widget setup -------- */
	/* ---------------------------- */
	
	function ducj_feat_Widget() {
	
		/* Widget settings */
		$widget_ops = array( 'classname' => 'ducj_feat_widget', 'description' => __('A ducj widget that displays the featured posts from your selected category', 'ducj') );

		/* Widget control settings */
		//$control_ops = array( 'width' => 160, 'height' => 600, 'id_base' => 'thn_feat_widget' );

		/* Create the widget */
		$this->WP_Widget( 'ducj_feat_widget', __('Featured Posts Widget', 'ducj'), $widget_ops );
	}

	/* ---------------------------- */
	/* ------- Display Widget -------- */
	/* ---------------------------- */
	
	function widget( $args, $instance ) {
		extract( $args );

		/* Our variables from the widget settings. */
		$title = apply_filters('widget_title', $instance['title'] );
		$num = $instance['num'];
		$cat = $instance['cat'];

		/* Before widget (defined by themes). */
		echo $before_widget;

		/* Display the widget title if one was input (before and after defined by themes). */
		if ( $title )
			echo $before_title . $title . $after_title;
			
		/* Display a containing div */
		echo '<div class="thn_feat">';

		/* Display Posts */
		if ( $num )
		$popular = new WP_Query('ignore_sticky_posts=1&cat=' . $cat .' &posts_per_page=' . $num);
		echo '<ul>';
		while ($popular->have_posts()) : $popular->the_post();
		echo '<li>';
		echo '<a class="thn_wgt_thumb" href="' . get_permalink(get_the_ID()) . '" title="' . get_the_title() . '">';
		if ( has_post_thumbnail() ) :
		echo ''. the_post_thumbnail('thumbnail') . '';
		elseif($photo = ducj_get_images('numberposts=1', true)):
		echo ''. wp_get_attachment_image($photo[0]->ID , $size='thumbnail') . '';
		else :
		echo '<img src="'.get_template_directory_uri().'/images/blank_img2.png" alt="'.get_the_title().'" class="thumbnail"/>';
		endif;
		echo '</a>';
		echo '<div class="widget_content">';
		echo '<a class="thn_wgt_tt" href="' . get_permalink(get_the_ID()) . '" title="' . get_the_title() . '">' . get_the_title() . '</a><br />' ;
		echo ''. ducj_excerpt('ducj_excerptlength_index', 'ducj_excerptmore') . '';
		echo '</div>';
		echo '</li>';
    
		endwhile;
		
		echo '</ul>';
			
		echo '</div>';

		/* After widget (defined by themes). */
		echo $after_widget;
	}

	/* ---------------------------- */
	/* ------- Update Widget -------- */
	/* ---------------------------- */
	
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		/* Strip tags for title and name to remove HTML (important for text inputs). */
		$instance['title'] = strip_tags( $new_instance['title'] );

		/* No need to strip tags */
		$instance['num'] = $new_instance['num'];
		$instance['cat'] = $new_instance['cat'];

		return $instance;
	}
	
	/* ---------------------------- */
	/* ------- Widget Settings ------- */
	/* ---------------------------- */
	

	/**
	 * Displays the widget settings controls on the widget panel.
	 * Make use of the get_field_id() and get_field_name() function
	 * when creating your form elements. This handles the confusing stuff.
	 */
	
	function form( $instance ) {
	
		/* Set up some default widget settings. */
		$defaults = array(
		'title' => '',
		'num' => 3,
		'cat' => '',
		);
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<!-- Widget Title: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'ducj') ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" />
		</p>
        
        <!-- Category of Posts: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'cat' ); ?>"><?php _e('Category:', 'ducj') ?></label>
            <?php wp_dropdown_categories( array( 'name' => $this->get_field_name('cat'), 'selected' => $instance['cat'] ) ); ?>
			
		</p>

		<!-- Number of Posts: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'num' ); ?>"><?php _e('Number of Posts:', 'ducj') ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'num' ); ?>" name="<?php echo $this->get_field_name( 'num' ); ?>" value="<?php echo $instance['num']; ?>" />
		</p>
		
		
	<?php
	}

}

