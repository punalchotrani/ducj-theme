<?php
/*
Template Name: Galerie
*/
?>
 
<?php get_header(); ?>
 
            <div id="content-galerie">
 
<?php
$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
query_posts( array(
'post_type' => 'bilder',
// 'posts_per_page' => 10,
'caller_get_posts' => 10,
'paged' => $paged )
);
if (have_posts()) : while (have_posts()) : the_post(); ?>
<div>
<div class="gallery_content">
<div class="post-title-galerie"><h2><?php the_title(); ?></h2></div>
 
 <?php global $more; $more = 0; the_content('read more &raquo;'); ?>
 
</div>
</div>
<?php endwhile; ?>
 
<?php endif; ?>
 
</div><!-- #content -->
 
<?php get_footer(); ?>
