<?php
/**
 * font
 */
?>





<style type="text/css">
 
 @import url(http://fonts.googleapis.com/css?family=<?php echo of_get_option('font_select'); ?>);
 @import url(http://fonts.googleapis.com/css?family=Yanone+Kaffeesatz);
 
@import url(http://fonts.googleapis.com/css?family=Atomic+Age);
@import url(http://fonts.googleapis.com/css?family=Bad+Script);
@import url(http://fonts.googleapis.com/css?family=Delius+Swash+Caps);
@import url(http://fonts.googleapis.com/css?family=Droid+Sans);

@import url(http://fonts.googleapis.com/css?family=Loved+by+the+King);
@import url(http://fonts.googleapis.com/css?family=Marck+Script);
@import url(http://fonts.googleapis.com/css?family=Nova+Slim);
@import url(http://fonts.googleapis.com/css?family=Roboto+Slab);
@import url(http://fonts.googleapis.com/css?family=Signika+Negative);
@import url(http://fonts.googleapis.com/css?family=Sonsie+One);



@font-face {
	font-family: '<?php echo of_get_option('font_select'); ?>';

	font-weight: normal;
	font-style: normal;
}


h1, h2, h3, h4, h5, h6, .trt_button a, #submit_msg, #submit_msg, #submit, .fourofour a, .trt_wgt_tt, #related ul li a, .logo h1 a, #slider .slider-content h2 a, .lay1 .post .postitle a, .lay1 .page .postitle a, #posts .postitle, #posts .postitle a, .znn_wgt_tt,#site-title a,.header a,.desc ,#site-title2 a { font-family: '<?php echo of_get_option('font_select'); ?>'!important; font-weight:normal!important; text-transform:capitalize!important;}


.logo h1 a{ font-size:30px;}
#topmenu ul li a{ font-size:14px;font-family: '<?php echo of_get_option('font_select'); ?>'!important; font-weight:normal!important; text-transform:capitalize!important;}


#thn_welcom{font-family: '<?php echo of_get_option('font_select'); ?>'!important; font-size:20px;}
#footer .widgets .widget ul li .thn_wgt_tt, #sidebar .widgets .widget li a, .commentlist .commentmetadata, .commentlist .commentmetadata a, #newslider_home ul#tabs_home li a h4, ul#tabs li a{font-family: '<?php echo of_get_option('font_select'); ?>'!important; font-size:14px;}
.single_wrap .postitle{ font-size:20px;}





 </style>

 